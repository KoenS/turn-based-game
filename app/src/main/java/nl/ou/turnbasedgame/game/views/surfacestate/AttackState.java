package nl.ou.turnbasedgame.game.views.surfacestate;

import nl.ou.turnbasedgame.game.models.Game;
import nl.ou.turnbasedgame.game.models.grid.Tile;
import nl.ou.turnbasedgame.game.views.GameSurface;

public class AttackState implements SurfaceState {
    /**
     * Handles the touchevent when the gameSurface is in the default state.
     *
     * When the player touches a tile within movement range, then the player should move to that
     * tile.
     *
     * @param tile the tile that has been touched
     * @param game the game object
     * @param surface the gamesurface
     */
    @Override
    public void handleOnTouch(Tile tile, Game game, GameSurface surface) {
        if(game.isValidAttackTargetForControlledPlayer(tile)){
            game.createAttackAction(tile.getRow(), tile.getCol());
        }
        //Resets the gameboard to the default state
        game.getGameboard().resetGrid();
        surface.setSurfaceState(new DefaultState());
    }
}
