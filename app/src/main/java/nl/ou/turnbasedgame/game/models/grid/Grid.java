package nl.ou.turnbasedgame.game.models.grid;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import nl.ou.turnbasedgame.game.models.player.AbstractPlayer;

/**
 * The Grid class represents the game grid.
 * The default grid has a width of 8.
 * The default grid has a heihgt of 8.
 *
 * The tile at the top left is at position (0,0).
 * The tile at the bottom right is at position (7,7).
 */
public class Grid {

    //Default grid width and height
    private static final int DEFAULT_WIDTH = 8;
    private static final int DEFAULT_HEIGHT = 8;

    //An array of all the tiles within the grid
    private Tile[][] tiles;
    private List<AbstractPlayer> players;

    private int rows;
    private int cols;

    /**
     * Constructor for the grid class.
     */
    public Grid(List<AbstractPlayer> players) {
        rows = DEFAULT_HEIGHT;
        cols = DEFAULT_WIDTH;
        initGrid(rows, cols);
        this.players = players;
    }

    /**
     * Creates a new grid.
     * @param rows number of rows
     * @param cols number of columns
     */
    private void initGrid(int rows, int cols){
        tiles = new Tile[rows][cols];
        for (int current_row = 0; current_row < getRowCount(); current_row++) {
            for (int current_col = 0; current_col < getColCount(); current_col++) {
                Tile tile = new Tile(current_row, current_col);
                tiles[current_row][current_col] = tile;
            }
        }
    }

    /**
     * Resets the color of every tile in the grid.
     */
    public void resetGrid(){
        for (int current_row = 0; current_row < getRowCount(); current_row++) {
            for (int current_col = 0; current_col < getColCount(); current_col++) {
                Tile tile = getTile(current_row, current_col);
                tile.resetColor();
            }
        }
    }

    public List<Tile> getTileList(){
        List<Tile> tiles = new ArrayList<>();
        for (int current_row = 0; current_row < getRowCount(); current_row++) {
            for (int current_col = 0; current_col < getColCount(); current_col++) {
                tiles.add(getTile(current_row,current_col));
            }
        }
        return tiles;
    }

    /**
     * get the number of rows in the grid.
     * @return number of rows in the grid.
     */
    public int getRowCount(){
        return tiles.length;
    }

    /**
     * get the number of columns in the grid.
     * @return number of columns in the grid.
     */
    public int getColCount(){
        return tiles[0].length;
    }

    /**
     * Returns a tile at a given position.
     * @param row the row of the tile.
     * @param col the column of the tile.
     * @return a tile.
     */
    public Tile getTile(int row, int col){
        return tiles[row][col];
    }

    /**
     * Marks all the tiles that the selected can move to.
     * @param player player
     */
    public void markMovableTiles(AbstractPlayer player) {
        this.resetGrid();

        List<Tile> tiles = getMovableTiles(player);
        for (Tile tile : tiles) {
            tile.setColorToMovable();
        }
    }

    /**
     * Gets a list of tiles that can be attacked by the player
     *
     * A player can move to any tile that is within the movement range of the player and not
     * occupied by another player.
     *
     * @return a list of tiles that the player can move towards
     */
    public List<Tile> getMovableTiles(AbstractPlayer player){
        int movement = player.getMovement();
        int row = player.getRow();
        int col = player.getCol();

        List<Tile> moveAbleTiles = getTilesWithinRange(row, col, movement, false, true);

        return moveAbleTiles;
    }

    /**
     * Marks all the tiles that the player can attack.
     * @param player player
     */
    public void markAttackableTiles(AbstractPlayer player) {
        this.resetGrid();

        List<Tile> tiles = getAttackableTiles(player);
        for (Tile tile : tiles) {
            tile.setColorToAttackable();
        }
    }

    /**
     * Gets a list of tiles that can be attacked by the player
     * <p>
     * A player can attack a tile that is within the attack range of the player and occupied by
     * another player.
     *
     * @return a list of tiles that can be attacked by the player
     */
    public List<Tile> getAttackableTiles(AbstractPlayer player) {
        int range = player.getRange();
        int row = player.getRow();
        int col = player.getCol();

        List<Tile> tiles = getTilesWithinRange(row, col, range, true, false);
        //remove the player occupied tile form attackable tiles
        //the player should not be able to attack itself.
        Tile playerTile = getTile(player.getRow(),player.getCol());
        tiles.remove(playerTile);

        return tiles;
    }

    /**
     * Gets a list of all the empty tiles
     * @return a list of empty tiles
     */
    public List<Tile> getEmptyTiles() {
        List<Tile> emptyTiles = this.getTileList();

        for(Tile occupiedTile: getOccupiedTiles()){
            emptyTiles.remove(occupiedTile);
        }

        return emptyTiles;
    }

    /**
     * Gets a list of all the occupied tiles
     * @return a list of occupies tiles
     */
    public List<Tile> getOccupiedTiles(){
        List<Tile> occupiedTiles = new ArrayList<>();

        for(AbstractPlayer player: players){
            occupiedTiles.add(this.getTile(player.getRow(),player.getCol()));
        }

        return occupiedTiles;
    }

    /**
     * Checks whether a given tile is empty.
     * @param tile the tile that will be checked.
     * @return a boolean indicating whether a tile is empty
     */
    /*public boolean tileIsEmpty(Tile tile) {
        return !getOccupiedTiles().contains(tile);
    }*/

    /**
     * Get a random empty tile
     *
     * @return an empty tile.
     */
    public Tile getRandomEmptyTile() {
        List<Tile> emptyTiles = getEmptyTiles();
        return emptyTiles.get(new Random().nextInt(emptyTiles.size()));
    }

    /*
     * Disable method as it is not used anymore right now
    public Tile getNextTile(Tile tile){
        int col = tile.getCol();
        int row = tile.getRow();

        if((col + 1) >= this.getColCount() && (row + 1) >= this.getRowCount()){
            col = 0;
            row = 0;
        } else if((col + 1) >= this.getColCount()){
            col = 0;
            row = row++;
        } else{
            col = col + 1;
        }

        return this.getTile(row, col);
    }
     */

    /**
     * Gets a list of tiles that are within a certain distance of the player.
     * @param row the row
     * @param col the col
     * @param distance the distance
     * @param excludeEmpty exclude empty tiles from the result
     * @param excludeOccupied exclude occupied tiles from the result
     * @return a list of tiles that are within a certain distance of the player.
     */
    public List<Tile> getTilesWithinRange(int row, int col, int distance, boolean excludeEmpty, boolean excludeOccupied){
        Set<Tile> tileSet = new HashSet<>();
        Grid grid = this;

        for(int i = 0; i < (distance + 1); i++){
            for(int j = 0; j < (distance - i + 1); j++){
                Tile tile = null;

                if(row - i >= 0 && col - j >= 0){
                    tile = grid.getTile(row - i, col - j);
                    if (!(excludeEmpty && getEmptyTiles().contains(tile)) &&
                        !(excludeOccupied && getOccupiedTiles().contains(tile))) {
                        tileSet.add(tile);
                    }
                }
                if(row + i < grid.getRowCount() && col - j >= 0){
                    tile = grid.getTile(row + i, col - j);
                    if (!(excludeEmpty && getEmptyTiles().contains(tile)) &&
                            !(excludeOccupied && getOccupiedTiles().contains(tile))) {
                        tileSet.add(tile);
                    }
                }
                if(row - i >= 0 && col + j < grid.getColCount()){
                    tile = grid.getTile(row - i, col + j);
                    if (!(excludeEmpty && getEmptyTiles().contains(tile)) &&
                            !(excludeOccupied && getOccupiedTiles().contains(tile))) {
                        tileSet.add(tile);
                    }
                }
                if(row + i < grid.getRowCount() && col + j < grid.getColCount()){
                    tile = grid.getTile(row + i, col + j);
                    if (!(excludeEmpty && getEmptyTiles().contains(tile)) &&
                            !(excludeOccupied && getOccupiedTiles().contains(tile))) {
                        tileSet.add(tile);
                    }
                }
            }
        }

        return new ArrayList<>(tileSet);
    }
}
