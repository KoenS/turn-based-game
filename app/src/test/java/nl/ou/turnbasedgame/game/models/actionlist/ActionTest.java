package nl.ou.turnbasedgame.game.models.actionlist;

import org.junit.Test;


import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertNotEquals;


public class ActionTest {

    @Test
    public void testEqualActions(){
        Action action1 = new Action(ActionType.MOVE, "f8567dce-fd91-452a-be8e-252b663fcdb6", 1,1, "player1", 1559557758432L);
        Action action2 = new Action(ActionType.MOVE, "f8567dce-fd91-452a-be8e-252b663fcdb6", 1,1, "player1", 1559557758432L);

        assertEquals("action1 should equal action2", action1, action2);
        assertEquals("action2 should equal action1", action2, action1);
    }

    @Test
    public void testUnEqualActions(){
        Action action1 = new Action(ActionType.MOVE, "anotherID", 1,1, "player1", 1559557758432L);
        Action action2 = new Action(ActionType.MOVE, "f8567dce-fd91-452a-be8e-252b663fcdb6", 1,1, "player1", 1559557758432L);

        assertNotEquals("action1 should not equal action2", action1, action2);
    }

}
