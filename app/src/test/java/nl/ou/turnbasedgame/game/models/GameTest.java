package nl.ou.turnbasedgame.game.models;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import nl.ou.turnbasedgame.R;
import nl.ou.turnbasedgame.game.models.actionlist.Action;
import nl.ou.turnbasedgame.game.models.actionlist.ActionList;
import nl.ou.turnbasedgame.game.models.actionlist.ActionType;
import nl.ou.turnbasedgame.game.models.grid.Grid;
import nl.ou.turnbasedgame.game.models.grid.Tile;
import nl.ou.turnbasedgame.util.observer.Observer;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class GameTest {


    private Game game;

    @Mock
    private GameLobby gameLobby;

    @Mock
    private ActionList actionList;

    @Mock
    private Action joinAction, moveAction, attackAction, leaveAction;

    @Mock
    private Observer observer;

    private final String GAMENAME = "testGame";
    private final String ACTIONLIST = "-LhMYN_DLjvEF1QO_yVq";
    private final String PLAYERNAME_1 = "player1";
    private final String PLAYERNAME_2 = "player2";
    private final String PLAYERNAME_3 = "player3";


    private final int HUMAN_DEFAULT_IMAGE_RESOURCE = R.drawable.ic_pikeman;
    private final int HUMAN_CONTROLLER_IMAGE_RESOURCE = R.drawable.ic_pikeman_green;



    @Before
    public void setUp(){
        Mockito.when(gameLobby.getName()).thenReturn(GAMENAME);
        Mockito.when(gameLobby.getActionList()).thenReturn(ACTIONLIST);

        game = new Game(gameLobby, PLAYERNAME_1);


        //Let player 1 join the game at tile (4,4)
        Mockito.when(joinAction.getPlayer()).thenReturn(PLAYERNAME_1);
        Mockito.when(joinAction.getRow()).thenReturn(4);
        Mockito.when(joinAction.getCol()).thenReturn(4);
        game.performJoinAction(joinAction);

        //Let player 3 join the game at tile (4,5)
        Mockito.when(joinAction.getPlayer()).thenReturn(PLAYERNAME_3);
        Mockito.when(joinAction.getRow()).thenReturn(4);
        Mockito.when(joinAction.getCol()).thenReturn(5);
        game.performJoinAction(joinAction);

        game.registerObserver(observer);

        game.setActionList(actionList);
    }

    @Test
    public void testGetGameName(){
        assertEquals("getGameName should return gameName", game.getGameName(), GAMENAME);
    }

    @Test
    public void testGetGameBoard(){
        assertThat(game.getGameboard(), Matchers.any(Grid.class));
    }

    @Test
    public void testGetActionListName(){
        assertEquals(game.getActionListName(), ACTIONLIST);
    }

    @Test
    public void testFinishedLoadingActionList(){
        game.finishedLoadingActionList();
        //test whether the observer gets notified when the actionList finished loading
        verify(observer, times(1)).update();
    }

    @Test
    public void testInitialLoadingActionList(){
        //observer should not be notified when it loading the game.
        game.startLoadingActionList();

        //perform an action
        Mockito.when(joinAction.getPlayer()).thenReturn(PLAYERNAME_1);
        Mockito.when(joinAction.getRow()).thenReturn(2);
        Mockito.when(joinAction.getCol()).thenReturn(1);

        game.performMoveAction(joinAction);

        verify(observer, times(0)).update();


        //game is finished loading the action list, observer should be notified now.
        game.finishedLoadingActionList();

        verify(observer, times(1)).update();

        //perform another action which should notify the observer again
        Mockito.when(moveAction.getPlayer()).thenReturn(PLAYERNAME_1);
        Mockito.when(moveAction.getRow()).thenReturn(3);
        Mockito.when(moveAction.getCol()).thenReturn(3);

        game.performMoveAction(moveAction);

        //The observer should only be notified 1 time.
        verify(observer, times(2)).update();
    }

    @Test
    public void testPerformJoinAction(){
        Mockito.when(joinAction.getPlayer()).thenReturn(PLAYERNAME_2);
        Mockito.when(joinAction.getRow()).thenReturn(2);

        Mockito.when(joinAction.getCol()).thenReturn(1);

        assertThat("The player list should contain 2 players before a join action", game.getPlayers(), hasSize(2));

        game.performJoinAction(joinAction);
        assertEquals("The player list should contain the player with name PLAYERNAME_2", PLAYERNAME_2, game.getPlayers().get(2).getName());

        assertEquals("The player should occupy the tile (2,1)", 2, game.getPlayers().get(2).getRow());
        assertEquals("The player should occupy the tile (2,1)",  1, game.getPlayers().get(2).getCol());

        assertEquals(game.getPlayers().get(2).getImageResource(), HUMAN_DEFAULT_IMAGE_RESOURCE);

        //verify that the observer gets notified
        verify(observer, times(1)).update();


    }



    @Test
    public void testJoinGame(){
        game.joinGame();
        verify(actionList, times(1)).addAction(eq(PLAYERNAME_1), eq(ActionType.JOIN), anyInt(), anyInt());
    }

    @Test
    public void testPerformMoveAction(){
        Mockito.when(moveAction.getPlayer()).thenReturn(PLAYERNAME_1);
        Mockito.when(moveAction.getRow()).thenReturn(3);
        Mockito.when(moveAction.getCol()).thenReturn(3);

        game.performMoveAction(moveAction);

        assertEquals("The player should occupy the tile (3,3)",3, game.getPlayers().get(0).getRow());
        assertEquals("The player should occupy the tile (3,3)", 3, game.getPlayers().get(0).getCol());

        //verify that the observer gets notified
        verify(observer, times(1)).update();
    }

    @Test
    public void testCreateMoveActionGame(){
        game.createMoveAction(3,3);
        verify(actionList, times(1)).addAction(PLAYERNAME_1, ActionType.MOVE, 3, 3);
    }

    @Test
    public void performAttackAction(){
        //Player 1 attacks player 3
        Mockito.when(attackAction.getPlayer()).thenReturn(PLAYERNAME_1);
        Mockito.when(attackAction.getRow()).thenReturn(4);
        Mockito.when(attackAction.getCol()).thenReturn(5);

        game.performAttackAction(attackAction);

        //verify that the observer gets notified
        verify(observer, times(1)).update();
    }

    @Test
    public void testCreateAttackActionGame(){
        game.createAttackAction(3,4);
        verify(actionList, times(1)).addAction(PLAYERNAME_1, ActionType.ATTACK, 3, 4);
    }

    @Test
    public void testCreateLeaveAction(){
        game.createLeaveAction();
        verify(actionList, times(1)).addAction(eq(PLAYERNAME_1), eq(ActionType.LEAVE), anyInt(), anyInt());
    }

    @Test
    public void testPerformLeaveAction(){
        Mockito.when(leaveAction.getPlayer()).thenReturn(PLAYERNAME_1);

        game.performLeaveAction(leaveAction);
        //When player 1 leaves there should only be one player left in the game.
        assertThat(game.getPlayers(), hasSize(1));
        verify(observer, times(1)).update();
    }

    @Test
    public void testRemoveActionListEventListeners(){
        game.removeActionListEventListeners();

        verify(actionList, times(1)).removeChildEventListener();
    }

    @Test
    public void testControllerCanPerformAction(){
        //player1 performs a move action
        Mockito.when(moveAction.getPlayer()).thenReturn(PLAYERNAME_1);
        Mockito.when(moveAction.getRow()).thenReturn(3);
        Mockito.when(moveAction.getCol()).thenReturn(3);
        game.performMoveAction(moveAction);

        assertFalse("The player shouldn't be able to perform an action after it has performed an action",
                game.controllerCanPerformAction());

        //player3 performs a move action
        Mockito.when(moveAction.getPlayer()).thenReturn(PLAYERNAME_3);
        Mockito.when(moveAction.getRow()).thenReturn(3);
        Mockito.when(moveAction.getCol()).thenReturn(3);
        game.performMoveAction(moveAction);

        assertTrue("The player should be able to perform an action after both players have performed an action",
                game.controllerCanPerformAction());
    }

    @Test
    public void testPerformJoinActionController(){
        //When the controller joins, the player stored in controlledPlayer should be the same object
        //as the player in Player list

        //First we make player1 leave
        Mockito.when(leaveAction.getPlayer()).thenReturn(PLAYERNAME_1);

        game.performLeaveAction(leaveAction);

        //Then we create a second join action for player1
        Mockito.when(joinAction.getPlayer()).thenReturn(PLAYERNAME_1);
        Mockito.when(joinAction.getRow()).thenReturn(2);
        Mockito.when(joinAction.getCol()).thenReturn(1);

        game.performJoinAction(joinAction);

        assertEquals("controlled player and the second player in the playerlist should be the same object",
                game.getControlledPlayer(),
                game.getPlayers().get(1));

        //The player should get the controller image resource
        assertEquals(game.getControlledPlayer().getImageResource(), HUMAN_CONTROLLER_IMAGE_RESOURCE);
    }

    @Test
    public void testIsValidMoveForControlledPlayer(){
        //Tile (4,5) is occupied by Player 3
        Tile target = game.getGameboard().getTile(4,5);

        assertFalse(game.isValidMoveForControlledPlayer(target));

        //Tile (4,3) is empty and thus a valid target
        target = game.getGameboard().getTile(4,3);
        assertTrue(game.isValidMoveForControlledPlayer(target));
    }



    @Test
    public void testIsValidAttackTargetForControlledPlayer(){
        //Tile (4,5) is occupied by Player 3 and thus a valid attack target
        Tile target = game.getGameboard().getTile(4,5);
        assertTrue(game.isValidAttackTargetForControlledPlayer(target));

        //Tile (4,3) is empty and thus a not valid target
        target = game.getGameboard().getTile(4,3);
        assertFalse(game.isValidAttackTargetForControlledPlayer(target));

    }



}
