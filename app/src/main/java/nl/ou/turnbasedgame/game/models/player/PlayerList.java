package nl.ou.turnbasedgame.game.models.player;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

import nl.ou.turnbasedgame.game.models.Game;
import nl.ou.turnbasedgame.game.models.grid.Tile;


/**
 * A class that represents the playerList by the game.
 * The playerlist is stored in a firebase database.
 * This class is responsible for pulling all players from the database.
 * Players are put inside the database by the joinGame or createGame activities.
 * <p>
 * The playerList is a list of all players participating in the game.
 */
public class PlayerList {

    private final String TAG = "PlayerList";

    private Game game; // Used to pass new players to the game controller.

    private List<AbstractPlayer> players = new ArrayList<>(); //A list of players in the game.
    private List<String> playersKeys = new ArrayList<>();     //A list of player keys .

    private FirebaseDatabase database = FirebaseDatabase.getInstance();
    private DatabaseReference valueRef;

    //String constants used find the right location in the database.
    private static String PLAYERLIST_PATHNAME = "playerList/";
    private static String PLAYERS_PATHNAME = "players/";

    /* Constructor ********************************************************************************/

    /**
     * The constructor of the PlayerList
     * @param game the game that belongs to this playerList.
     */
    public PlayerList(Game game, String gameKey) {
        this.game = game;

        //The path where the playerList is located in the database.
        valueRef = database.getReference(PLAYERLIST_PATHNAME + gameKey + "/" + PLAYERS_PATHNAME);
    }

    /* Event listeners ****************************************************************************/

    /**
     * The ChildEventListener that checks whether an player has been added to the actionList.
     */
    private ChildEventListener playerListChildEventListener = new ChildEventListener() {
        @Override
        public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
            Log.d(TAG, "Player added: " + dataSnapshot.toString());

            AbstractPlayer player = dataSnapshot.getValue(AbstractPlayer.class);
            String playerKey = dataSnapshot.getKey();

            // recast the object to the correct extended class.
            if (player.getType().equals("Dragon")) {
                player = dataSnapshot.getValue(DragonPlayer.class);
            } else if (player.getType().equals("Human")) {
                player = dataSnapshot.getValue(HumanPlayer.class);
            }

            players.add(player);
            playersKeys.add(playerKey);

            //todo: notify game of an update
            //notifyItemInserted(players.size() - 1);
        }

        @Override
        public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
            Log.d(TAG, "Player changed: " + dataSnapshot.toString());

            AbstractPlayer player = dataSnapshot.getValue(AbstractPlayer.class);
            String playerKey = dataSnapshot.getKey();

            int index = playersKeys.indexOf(playerKey);
            if (index > -1 ) {
                // recast the object to the correct extended class.
                if (player.getType().equals("Dragon")) {
                    player = dataSnapshot.getValue(DragonPlayer.class);
                } else if (player.getType().equals("Human")) {
                    player = dataSnapshot.getValue(HumanPlayer.class);
                }
                players.set(index, player);

                //todo: notify game of an update
                //notifyItemInserted(players.size() - 1);
            } else {
                Log.w(TAG, "Player changed but not found: " + playerKey);
            }
        }

        @Override
        public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
            // A player should not be removed.
            Log.e(TAG, "Player removed from playerList...");
        }

        @Override
        public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
            // Nothing should happen when a child's priority has changed.
            Log.d(TAG, "Player moved in the playerList...");
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {
            //When an error happens, we could just reload the game
            Log.w(TAG, "playerListChildEventListener onCancelled", databaseError.toException());
        }
    };

    /* Properties *********************************************************************************/

    public List<AbstractPlayer> getPlayers() {
        return players;
    }

    public AbstractPlayer getPlayer(String name) {
        for (AbstractPlayer player : players) {
            if (player.getName().equals(name)) {
                return player;
            }
        }
        return null;
    }

    public AbstractPlayer getPlayer(Tile tile) {
        for (AbstractPlayer player : players) {
            if (tile.getRow() == player.getRow() && tile.getCol() == player.getCol()) {
                return player;
            }
        }
        return null;
    }
}

