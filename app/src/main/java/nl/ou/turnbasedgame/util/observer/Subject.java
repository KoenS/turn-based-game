package nl.ou.turnbasedgame.util.observer;


/**
 * Subject interface used for the observer pattern
 */
public interface Subject {
    public void registerObserver(Observer observer);
    public void unregisterObserver(Observer observer);
    public void notifyObserversObserver();
}
