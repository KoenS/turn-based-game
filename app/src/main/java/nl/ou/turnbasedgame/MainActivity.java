package nl.ou.turnbasedgame;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;

public class MainActivity extends BaseActivity {

	private static final String TAG = "MainActivity";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// fix the text color of the system bar. This should be done in the styles of the base-library.
		getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);

		Toolbar toolbar = findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);

		initButtons();
	}

	private void initButtons(){
		initResumeGameButton();
		Button gotoLobbyButton =  findViewById(R.id.goto_lobby_button);
		gotoLobbyButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				try {
					Intent intent = new Intent(getBaseContext(), LobbyActivity.class);
					startActivity(intent);
				} catch(Exception e) {
					Log.e(TAG, "Goto Lobby", e);
				}
			}
		});
	}

	/**
	 * Adds a button that allows the player to resume an old game in case the application has
	 * closed unexpectedly.
	 */
	private void initResumeGameButton(){
		Log.i(TAG, "initResumeGameButton");
		Button resumeGameButton = findViewById(R.id.resume_game_button);

		SharedPreferences sharedPref  = getSharedPreferences(GameActivity.GAME_SHAREDPREF_NAME, Context.MODE_PRIVATE);

		Log.d(TAG, "EXTRA_GAME_KEY value: " + sharedPref.getString(GameActivity.EXTRA_GAME_KEY , null));
 		if(sharedPref.contains(GameActivity.EXTRA_GAME_KEY) && sharedPref.contains(GameActivity.EXTRA_PLAYER_NAME) ){

			String gameKey = sharedPref.getString(GameActivity.EXTRA_GAME_KEY, null);
			String playerName = sharedPref.getString(GameActivity.EXTRA_PLAYER_NAME, null);

			resumeGameButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					Intent intent = new Intent(getBaseContext(), GameActivity.class);

					// Pass game key and player name to the GameActivity.
					intent.putExtra(GameActivity.EXTRA_GAME_KEY, gameKey);
					intent.putExtra(GameActivity.EXTRA_PLAYER_NAME, playerName);
					startActivity(intent);
				}
			});
		} else{
			Log.i(TAG, "resumeGameButton.setVisibility(View.INVISIBLE)");

			resumeGameButton.setVisibility(View.INVISIBLE);
		}



	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();

		if (id == R.id.action_settings) {
			Intent intent = new Intent(this, SettingsActivity.class);
			//ActivityCompat.startActivity(this, intent, null);
			//supportFinishAfterTransition();
			TaskStackBuilder builder = TaskStackBuilder.create(this);
			builder.addNextIntentWithParentStack(intent);
			builder.startActivities();
			return true;
		}

		return super.onOptionsItemSelected(item);
	}
}
