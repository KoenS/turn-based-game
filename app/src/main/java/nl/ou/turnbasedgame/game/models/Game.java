package nl.ou.turnbasedgame.game.models;

import java.util.ArrayList;
import java.util.List;

import nl.ou.turnbasedgame.game.models.actionlist.Action;
import nl.ou.turnbasedgame.game.models.actionlist.ActionList;
import nl.ou.turnbasedgame.game.models.actionlist.ActionType;
import nl.ou.turnbasedgame.game.models.grid.Grid;
import nl.ou.turnbasedgame.game.models.grid.Tile;
import nl.ou.turnbasedgame.game.models.player.AbstractPlayer;
import nl.ou.turnbasedgame.game.models.player.DragonPlayer;
import nl.ou.turnbasedgame.game.models.player.HumanPlayer;
import nl.ou.turnbasedgame.util.observer.Observer;
import nl.ou.turnbasedgame.util.observer.Subject;

public class Game implements Subject {

    private final String LOG_TAG = "Game";


    //A list of players in the game.
    private List<AbstractPlayer> players = new ArrayList<>();

    //The player that is controlled by this  device
    private AbstractPlayer controlledPlayer;

    private ActionList actionList;
    private String playerName;
    private String gameName;
    private String actionListName;
    private List<Observer> observers = new ArrayList<>();

    //The gameboard (grid) on which the game is played.
    private Grid gameboard;

    //A boolean indicating whether the game is still loading the actionList
    private boolean gameLoadingInitialActionList = false;

    /**
     * Constructor of the game class. Load an existing game from database
     */
    public Game(GameLobby gameLobby, String playerName) {
        this.gameName = gameLobby.getName();
        this.actionListName = gameLobby.getActionList();

        this.playerName = playerName;
        this.gameboard = new Grid(players);

        //todo: remove hardcoded dragon name.
        if (playerName.equals("Smaug")) {
            controlledPlayer = new DragonPlayer(playerName, false);
        } else {
            controlledPlayer = new HumanPlayer(playerName, false);
        }
        //addPlayer(controlledPlayer);

    }

    /**
     * Setter for the actionList
     * @param actionList
     */
    public void setActionList(ActionList actionList){
        this.actionList = actionList;
    }

    /**
     * Creates a join action for the player
     */
    public void joinGame(){
        createJoinAction(playerName);
    }





    /**
     * Get a list of the players in the game.
     *
     * @return a list of the players in the game.
     */
    public List<AbstractPlayer> getPlayers() {
        return this.players;
    }

    /**
     * Get a list of actions taken in the game.
     *
     * @return a list of the actions.
     */
    public String getActionListName() {
        return this.actionListName;
    }

    /**
     * Gets a player with the given name
     *
     * @param name the name of the player
     * @return a player with a given name of null if that player doesn't exist.
     */
    private AbstractPlayer getPlayer(String name) {
        for (AbstractPlayer player : players) {
            if (player.getName().equals(name)) {
                return player;
            }
        }
        return null;
    }

    /**
     * Get the gameboard.
     *
     * @return The gameboard
     */
    public Grid getGameboard() {
        return this.gameboard;
    }

    /**
     * Gets the player that is controlled by this device
     *
     * @return the player
     */
    public AbstractPlayer getControlledPlayer() {
        return controlledPlayer;
    }

    /**
     * Adds a player to the game.
     *
     * @param player the player that will be added to the game
     */
    private void addPlayer(AbstractPlayer player) {
        players.add(player);
    }

    public String getGameName() {
        return gameName;
    }

    /**
     * This method should be invoked when loading the actionList. This prevents the player from
     * seeing all updates when loading the actionList.
     *
     * notifyObserversObserver doesn't notify observers after this method has been called.
     */
    public void startLoadingActionList(){
        gameLoadingInitialActionList = true;
    }

    /**
     * This method should be invoked when the actionList has finished loading.
     *
     * notifyObserversObserver should notify observers after this method has been called.
     */
    public void finishedLoadingActionList(){
        gameLoadingInitialActionList = false;
        notifyObserversObserver();
    }

    /**
     * Removes a player from the game
     * @param player the name of the player
     */
    public void removePlayer(AbstractPlayer player){
        players.remove(player);
    }

    /**
     * Removes a player from the game
     * @param playerName the name of the player
     */
    private void removePlayer(String playerName) {
        AbstractPlayer player = getPlayer(playerName);
        if (player != null) {
            removePlayer(player);
        }
    }

    /* UI Stuff ***********************************************************************************/

    public boolean isValidMoveForControlledPlayer(Tile destination){
        List<Tile> moveableTiles = gameboard.getMovableTiles(controlledPlayer);
        return moveableTiles.contains(destination);
    }

    public boolean isValidAttackTargetForControlledPlayer(Tile target){
        List<Tile> attackableTiles = gameboard.getAttackableTiles(controlledPlayer);
        return attackableTiles.contains(target);
    }

    /* Actions ************************************************************************************/

    /**
     * Create a new join action and adds them to the action list.
     *
     * @param playerName the name of the player that joins the game
     */
    private void createJoinAction(String playerName) {
        //move the player to random empty tile
        Tile emptyTile = gameboard.getRandomEmptyTile();
        actionList.addAction(playerName, ActionType.JOIN, emptyTile.getRow(), emptyTile.getCol());
    }

    /**
     * Performs a join action.
     * Adds a player to the games player list and moves the player to an empty tile.
     * <p>
     * If a player (controlled by this device) gets placed on a occupied tile, the join action
     * will be canceled and a new join action will be created.
     *
     * @param action the action that needs to be performed.
     */
    public void performJoinAction(Action action) {
        //Get the player who joined.
        AbstractPlayer player = getPlayer(action.getPlayer());

        boolean isController = false;
        if(action.getPlayer().equals(playerName)){
            isController = true;
        }

        //If the player does not yet exists, create a new one.
        if (player == null) {
            //todo: remove hardcoded dragon name.
            if (action.getPlayer().equals("Smaug")) {
                player = new DragonPlayer(action.getPlayer(), isController);
            } else {
                player = new HumanPlayer(action.getPlayer(), isController);
            }
            addPlayer(player);

            //Replace the controlled player by the new player if a player with playerName joins the game.
            //This is necessary because otherwise the controlledplayer and the player in the players list
            //would be different objects.
            if(isController){
                controlledPlayer = player;
            }
        }

        //Get the tile where the player is spawning.
        Tile destinationTile = gameboard.getTile(action.getRow(), action.getCol());
        //Ignoring collisions is easier. Sharing is caring.
        //while(!gameboard.tileIsEmpty(destinationTile)){
        //    destinationTile = gameboard.getNextTile(destinationTile);
        //}

        //Move the player onto his tile
        movePlayer(getPlayer(action.getPlayer()), destinationTile);

        //getPlayer(action.getPlayer()).setHasPerformedAction(true);
        //attemptStartNextTurn();
        notifyObserversObserver();
    }

    /**
     * Create a new move action and adds them to the action list.
     *
     * @param row the player moves to this row
     * @param col the player moves to this column
     */
    public void createMoveAction(int row, int col) {
        actionList.addAction(getControlledPlayer().getName(), ActionType.MOVE, row, col);
    }

    /**
     * Performs a move action.
     *
     * @param action the action that needs to be performed
     */
    public void performMoveAction(Action action) {
        AbstractPlayer player = getPlayer(action.getPlayer());
        movePlayer(player, gameboard.getTile(action.getRow(), action.getCol()));
        getPlayer(action.getPlayer()).setHasPerformedAction(true);
        attemptStartNextTurn();
        notifyObserversObserver();
    }

    /**
     * Moves a player to a tile
     *
     * @param player      the player that is moving
     * @param destination the destination tile
     */
    private void movePlayer(AbstractPlayer player, Tile destination) {
        List<Tile> moveAbleTiles = gameboard.getEmptyTiles();

        for (Tile tile : moveAbleTiles) {
            if (destination.equals(tile)) {
                player.move(tile.getRow(), tile.getCol());
            }
        }
    }

    /**
     * Create a new Attack action and adds them to the action list.
     *
     * @param row the row that will be attacked
     * @param col the column that will be attacked
     */
    public void createAttackAction(int row, int col) {
        actionList.addAction(getControlledPlayer().getName(), ActionType.ATTACK, row, col);
    }

    /**
     * Performs an attack action.
     *
     * @param action the action that needs to be performed.
     */
    public void performAttackAction(Action action) {
        attackTile(getPlayer(action.getPlayer()), gameboard.getTile(action.getRow(), action.getCol()));
        getPlayer(action.getPlayer()).setHasPerformedAction(true);
        attemptStartNextTurn();
        notifyObserversObserver();
    }

    /**
     * Attacks a player on a tile
     *
     * @param attacker    the attacking player
     * @param destination the tile that gets attacked
     */
    private void attackTile(AbstractPlayer attacker, Tile destination) {
        AbstractPlayer attackedPlayer = null;

        // Find the attacked player
        for (AbstractPlayer player : this.players) {
            if (destination.getRow() == player.getRow() && destination.getCol() == player.getCol() && !(player.equals(attacker))) {
                attackedPlayer = player;
            }
        }

        // Hit the attacked player
        if (attackedPlayer != null) {
            attackedPlayer.takeDamage(attacker.getStrength());
            // If the player is dead, remove player
            if (attackedPlayer.isDead()) {
                removePlayer(attackedPlayer);
            }
        }
    }

    /**
     * Create a leave action.
     */
    public void createLeaveAction() {
        actionList.addAction(getControlledPlayer().getName(), ActionType.LEAVE, 0, 0);
    }

    /**
     * Perform a leave action.
     *
     * @param action the action that needs to be performed.
     */
    public void performLeaveAction(Action action) {
        removePlayer(action.getPlayer());
        attemptStartNextTurn();
        notifyObserversObserver();
    }

    public void removeActionListEventListeners(){
        this.actionList.removeChildEventListener();
    }

    /**
     * Reloads the game in case of a desync.
     * Removes all the eventlisteners from the actionList
     * Removes the old actionList
     * Removes all the players from the game.
     * Creates a new actionList
     */
    public void reloadGame(){
        //make sure that no futher actions can be performed from the old actionList
        this.actionList.gameReloadTriggerd();
        //remove the eventlistener
        this.actionList.removeChildEventListener();
        this.players = new ArrayList<>();
        this.actionList = new ActionList(this);
    }



    private boolean allPlayersHavePerformedAction(){
        for(AbstractPlayer player: players){
            if(!player.getHasPerformedAction()){
                return false;
            }
        }
        return true;
    }

    private void attemptStartNextTurn(){
        if(allPlayersHavePerformedAction()){
            for(AbstractPlayer player: players){
                player.setHasPerformedAction(false);
            }
        }
    }

    public boolean controllerCanPerformAction(){
        if(controlledPlayer != null) {
            return !controlledPlayer.getHasPerformedAction();
        }else{
            return false;
        }
    }


    @Override
    public void registerObserver(Observer observer) {
        if (!observers.contains(observer)) {
            observers.add(observer);
        }
    }

    @Override
    public void unregisterObserver(Observer observer) {
            observers.remove(observer);
    }

    @Override
    public void notifyObserversObserver() {
        //When the game is still loading the actionList. Don't notify the observer.
        if(gameLoadingInitialActionList){
            return;
        }

        for (Observer observer : observers) {
            observer.update();
        }
    }
}
