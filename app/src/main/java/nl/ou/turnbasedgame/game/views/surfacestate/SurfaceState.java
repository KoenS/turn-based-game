package nl.ou.turnbasedgame.game.views.surfacestate;

import nl.ou.turnbasedgame.game.models.Game;
import nl.ou.turnbasedgame.game.models.grid.Tile;
import nl.ou.turnbasedgame.game.views.GameSurface;

public interface SurfaceState {

    public void handleOnTouch(Tile tile, Game game, GameSurface surface);
}
