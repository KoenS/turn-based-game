package nl.ou.turnbasedgame.game.models.player;

import org.junit.Before;
import org.junit.Test;

import nl.ou.turnbasedgame.R;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;

public class AbstractPlayerTest {
    private static final int DEFAULT_HITPOINTS = 10;

    private static int defaultImageResource = R.drawable.ic_pikeman;
    private static int defaultControllerImageResource = R.drawable.ic_pikeman_green;

    private final String PLAYER_ONE_NAME = "player1";
    private final String PLAYER_TWO_NAME = "player2";

    private HumanPlayer player1, player2;

    @Before
    public void setUp() {

        //player1 is a controller
        player1 = new HumanPlayer(PLAYER_ONE_NAME, true);
        //player1 is a normal player
        player2 = new HumanPlayer(PLAYER_TWO_NAME, false);
    }

    @Test
    public void testGetName(){
        assertEquals(PLAYER_ONE_NAME, player1.getName());
    }

    @Test
    public void testSetName() {
        player1.setName("testName");
        assertEquals( "testName", player1.getName());

    }

    @Test
    public void testMove(){
        player1.move(1,2);

        assertEquals(1 , player1.getRow());
        assertEquals(2 , player1.getCol());
    }

    @Test
    public void testTakeDamage(){
        assertEquals("The player starts with the default amount of hitpoints",
                DEFAULT_HITPOINTS,
                player1.getHitpoints());


        player1.takeDamage(2);

        assertEquals("The player should have lost 2 hitpoints",
                (DEFAULT_HITPOINTS - 2),
                player1.getHitpoints());

        player1.takeDamage(8);


        assertEquals("The player should have 0 hitpoints",
                (DEFAULT_HITPOINTS - 10),
                player1.getHitpoints());

        assertTrue("The should be dead after losing all hitpoints",
                player1.isDead());


    }
}
