package nl.ou.turnbasedgame.game.models.actionlist;

/**
 * An enum of all the possible action types that can be performed.
 */
public enum ActionType {
    ATTACK, MOVE, JOIN, LEAVE
}
