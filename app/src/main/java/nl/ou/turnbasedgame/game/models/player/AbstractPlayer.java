package nl.ou.turnbasedgame.game.models.player;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.util.Log;

import com.google.firebase.database.Exclude;

import java.util.HashMap;
import java.util.Map;

import nl.ou.turnbasedgame.game.models.Game;

public class AbstractPlayer extends BaseObservable {

    //TODO: change default values based on gamerules
    private static final int DEFAULT_MOVE_RANGE = 2;
    private static final int DEFAULT_ATTACK_RANGE = 1;

    private static final int DEFAULT_HITPOINTS = 10;
    private static final int DEFAULT_ATTACK_STRENGTH = 1;

    // Logging elements
    private static final String TAG = "AbstractPlayer";

    //The name of the player
    private String name;

    //The player's combat attributes
    private int range;
    private int movement;
    private int strength;
    private int maxhitpoints;
    private Class type = this.getClass(); //todo: the class should tell all the stats

    //The player's position
    private int row;
    private int col;

    //The player's status
    private boolean isActive;
    private int damageTaken = 0;
    private boolean isDead = false;

    //Game resources
    protected int imageResource;
    //private Game game;
    private boolean hasPerformedAction = false;

    /**
     * Constructor for the AbstractPlayer
     */
    public AbstractPlayer(String name){
        this.setName(name);
        this.setActive(true);

        this.setRange(DEFAULT_ATTACK_RANGE);
        this.setMovement(DEFAULT_MOVE_RANGE);
        this.setMaxHitpoints(DEFAULT_HITPOINTS);
        this.setStrength(DEFAULT_ATTACK_STRENGTH);
    }

    /**
     * default constructor required for DataSnapshot.getValue(AbstractPlayer.class)
     */
    public AbstractPlayer(){
        // do nothing.
    }

    /**
     *  Create a map of the AbstractPlayer object to store in firebase.
     */
    @Exclude
    public Map<String, Object> toMap() {

        HashMap<String, Object> result = new HashMap<>();

        result.put("name", name);
        result.put("type", this.getClass());
        result.put("active", isActive);
        //result.put("created", ServerValue.TIMESTAMP); // Value should be assigned in constructor. See issue #69
        if (this.getClass() == DragonPlayer.class) {
            result.put("type", "Dragon");
        } else if (this.getClass() == HumanPlayer.class) {
            result.put("type", "Human");
        }

        result.put("range", range);
        result.put("movement", movement);
        result.put("maxhitpoints", maxhitpoints);
        result.put("strength", strength);

        Log.d(TAG, "Player mapped to: " + result);

        return result;
    }

    /* character properties ***********************************************************************/

    /**
     * Gives the name of the player
     * @return the name of the player
     */
    @Bindable
    public String getName(){
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Class getType(){
        return this.type;
    }

    public void setType(Class type) {
        this.type = type;
    }

    public int getRange() {
        return range;
    }

    public void setRange(int range) {
        this.range = range;
    }

    public int getMovement() {
        return movement;
    }

    public void setMovement(int movement) {
        this.movement = movement;
    }

    @Bindable
    public int getStrength() {
        return strength;
    }

    public void setStrength(int attackStrength) {
        this.strength = attackStrength;
    }

    @Bindable
    public int getHitpoints(){
        return maxhitpoints - damageTaken;
    }

    public void setMaxHitpoints(int maxhitpoints) {
        this.maxhitpoints = maxhitpoints;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public boolean isDead() {
        return isDead;
    }

    public boolean getHasPerformedAction() {
        return hasPerformedAction;
    }

    public void setHasPerformedAction(boolean hasPerformedAction){
        this.hasPerformedAction = hasPerformedAction;
    }

    /**
     * Gets the imageResource that belongs to this player.
     * @return a int that points to the imageResource
     */
    public int getImageResource(){
        return imageResource;
    }


    /* positioning and range **********************************************************************/

    /**
     * Gets the row of the position of the player.
     * @return the row
     */
    public int getRow(){
        return row;
    }

    /**
     * Gets the column of the position of the player.
     * @return the row
     */
    public int getCol(){
        return col;
    }

    /**
     * Moves the player to a given position
     * @param row the row
     * @param col the column
     */
    public void move(int row, int col){
        this.row = row;
        this.col = col;
    }

    /**
     * Deal damage to the player. If it is too much, the player will die.
     * @param damage the amount of damage
     */
    public void takeDamage(int damage){
        damageTaken += damage;
        if (damageTaken >= maxhitpoints) {
            isDead = true;
        }
    }


    /* todo: these things should be moved the the controller **************************************/








}
