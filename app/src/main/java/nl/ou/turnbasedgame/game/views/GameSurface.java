package nl.ou.turnbasedgame.game.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.support.v4.content.res.ResourcesCompat;
import android.text.TextPaint;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.util.AttributeSet;

import nl.ou.turnbasedgame.GameActivity;
import nl.ou.turnbasedgame.R;
import nl.ou.turnbasedgame.game.models.player.AbstractPlayer;
import nl.ou.turnbasedgame.game.models.Game;
import nl.ou.turnbasedgame.game.models.grid.Tile;
import nl.ou.turnbasedgame.game.views.surfacestate.DefaultState;
import nl.ou.turnbasedgame.game.views.surfacestate.SurfaceState;
import nl.ou.turnbasedgame.util.observer.Observer;


public class GameSurface extends SurfaceView implements SurfaceHolder.Callback, Observer {
    private static final int GRIDPADDING = 2;
    private static final int PLAYER_ICON_MARGIN = 32;
    private static final int TEXTHEIGHT = 32;
    private static final int INFO_ICON_SIZE = 32;
    private static final int TEXTPADDING = 8;
    private static final int FONTSIZE = 14;

    private GameActivity gameActivity;
    private Game game;
    private SurfaceState surfaceState = new DefaultState();



    public GameSurface(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        getHolder().addCallback(this);
    }

    public GameSurface(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        getHolder().addCallback(this);
    }

    public void setSurfaceState(SurfaceState surfaceState){
        this.surfaceState = surfaceState;
    }

    public void setGameActivity(GameActivity gameActivity){
        this.gameActivity = gameActivity;
    }

    public GameActivity getGameActivity(){
        return gameActivity;
    }

    /**
     * Draw method that is responsible for drawing the gamesurface on the screen.
     * @param canvas
     */
    @Override
    public void draw(Canvas canvas){
        super.draw(canvas);
        //white background.
        canvas.drawColor(Color.WHITE);
        if(game != null){
            if(game.getGameboard() != null)
                drawGrid(canvas);
            if(game.getPlayers() != null){
                drawPlayers(canvas);
            }
        }
    }

    public void setGame(Game game){
        this.game = game;
        surfaceChanged(getHolder(),0,getWidth(),getHeight());
    }



    /**
     * Draws the grid.
     * @param canvas
     */
    private void drawGrid(Canvas canvas) {
        int width = getWidth();
        int height = getHeight();

        int tileWidth = width/game.getGameboard().getColCount();
        int tileHeight = height/game.getGameboard().getRowCount();

        for(int row = 0; row < game.getGameboard().getRowCount(); row++){
            for(int col = 0; col < game.getGameboard().getColCount(); col++){
                Tile tile = game.getGameboard().getTile(row, col);

                Paint tileColor = new Paint();
                tileColor.setColor(tile.getColor());

                canvas.drawRect(tileWidth * col + GRIDPADDING,
                        tileHeight  * row + GRIDPADDING,
                        (tileWidth * col) + tileWidth - GRIDPADDING,
                        (tileHeight  * row) + tileHeight - GRIDPADDING,
                        tileColor);
            }
        }
    }

    private void drawPlayerIcons(Canvas canvas, AbstractPlayer player, int tileLeft, int tileTop, int tileRight, int tileBottom){
        Drawable playerIcon = ResourcesCompat.getDrawable(getResources(), player.getImageResource(), null);
        playerIcon.setBounds( tileLeft, tileTop + PLAYER_ICON_MARGIN, tileRight - PLAYER_ICON_MARGIN, tileBottom  );
        playerIcon.draw(canvas);
    }

    private void drawPlayerInfo(Canvas canvas, AbstractPlayer player, int tileLeft, int tileTop, int tileRight, int tileBottom){
        TextPaint textPaint = new TextPaint();

        //draw Strength Text
        String strengthText = Integer.toString(player.getStrength());
        textPaint.setAntiAlias(true);
        textPaint.setTextSize(FONTSIZE * getResources().getDisplayMetrics().density);
        int textWidth = (int) textPaint.measureText(strengthText);
        textPaint.setColor(Color.BLACK);
        canvas.drawText(strengthText,
                (tileRight - textWidth - TEXTPADDING),
                tileTop + TEXTHEIGHT + TEXTHEIGHT,
                textPaint);

        //draw sword
        Drawable sword = ResourcesCompat.getDrawable(getResources(), R.drawable.ic_sword, null);
        sword.setBounds(tileRight - textWidth - TEXTPADDING - INFO_ICON_SIZE,
                tileTop + TEXTHEIGHT,
                tileRight - textWidth - TEXTPADDING,
                tileTop + INFO_ICON_SIZE + TEXTHEIGHT);
        sword.draw(canvas);

        //draw Health Text
        String healthText = Integer.toString(player.getHitpoints());
        textPaint.setAntiAlias(true);
        textPaint.setTextSize(FONTSIZE * getResources().getDisplayMetrics().density);
        textWidth = (int) textPaint.measureText(healthText);
        textPaint.setColor(Color.BLACK);
        canvas.drawText(healthText,
                (tileRight - textWidth - TEXTPADDING),
                tileTop + TEXTHEIGHT,
                textPaint);

        //draw hearth
        Drawable hearth = ResourcesCompat.getDrawable(getResources(), R.drawable.ic_heart, null);
        hearth.setBounds(tileRight - textWidth - TEXTPADDING - INFO_ICON_SIZE,
                tileTop,
                tileRight - textWidth - TEXTPADDING,
                tileTop + INFO_ICON_SIZE);
        hearth.draw(canvas);
    }

    /**
     * Draws the players as black circles.
     * @param canvas
     */
    private void drawPlayers(Canvas canvas){
        int width = getWidth();
        int height = getHeight();

        int tileWidth = width/game.getGameboard().getColCount();
        int tileHeight = height/game.getGameboard().getRowCount();


        for(AbstractPlayer player: game.getPlayers()){
            int tileLeft = tileWidth * player.getCol() + GRIDPADDING;
            int tileTop = tileHeight  * player.getRow() + GRIDPADDING;
            int tileRight = (tileWidth * player.getCol()) + tileWidth + GRIDPADDING;
            int tileBottom = (tileHeight  * player.getRow()) + tileHeight + GRIDPADDING;

            drawPlayerIcons(canvas, player, tileLeft, tileTop, tileRight, tileBottom);
            drawPlayerInfo(canvas, player, tileLeft, tileTop, tileRight, tileBottom);

        }
    }

    /**
     * Callback that gets called when a uses touches the surface.
     * @param event
     * @return
     */
    @Override
    public boolean onTouchEvent(MotionEvent event){
        int width = getWidth();
        int height = getHeight();

        int tileWidth = width/game.getGameboard().getColCount();
        int tileHeight = height/game.getGameboard().getRowCount();

        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            int col = ((int)event.getX() / tileWidth);
            int row = ((int)event.getY() / tileHeight);

            Tile tile = game.getGameboard().getTile(row, col);

            surfaceState.handleOnTouch(tile, game, this);
            surfaceChanged(getHolder(),0,getWidth(),getHeight());
        }
        return true;
    }


    /**
     * Callback that gets called when the
     * @param holder
     */
    @Override
    public void surfaceCreated(SurfaceHolder holder) {
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        Canvas c = getHolder().lockCanvas();
        if(c != null){
            draw(c);
            getHolder().unlockCanvasAndPost(c);

        }
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder){
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int width = MeasureSpec.getSize(widthMeasureSpec);
        int height = MeasureSpec.getSize(heightMeasureSpec);
        int size;
        if(MeasureSpec.getMode(widthMeasureSpec) == MeasureSpec.EXACTLY ^ MeasureSpec.getMode(heightMeasureSpec) == MeasureSpec.EXACTLY) {
            if (MeasureSpec.getMode(widthMeasureSpec) == MeasureSpec.EXACTLY)
                size = width;
            else
                size = height;
        }
        else
            size = Math.min(width, height);
        setMeasuredDimension(size, size);
    }

    /**
     * updates the GameSurface.
     */
    public void update(){
        surfaceChanged(getHolder(),0,getWidth(),getHeight());
    }
}
