package nl.ou.turnbasedgame.game.models.grid;

import android.graphics.Color;

/**
 * The Tile class represents a tile within the game grid.
 */
public class Tile {
    //Default colors for the tiles.
    public static final int DEFAULT_COLOR = Color.LTGRAY;
    public static final int ATTACKABLE_COLOR = Color.RED;
    public static final int MOVABLE_COLOR = Color.BLUE;

    //The color of the tile

    private int color;

    //row and column of the tile
    private int row;
    private int col;


    /**
     * Constructor for the tile class.
     * @param row the row of the tile.
     * @param col the column of the tile.
     */
    public Tile(int row, int col){
        this.color = DEFAULT_COLOR;
        this.row =  row;
        this.col = col;
    }

    /**
     * Gets the current color of the tile.
     * @return an integer that represents the color.
     */
    public int getColor(){
        return this.color;
    }

    /**
     * Sets the color of the tile to the DEFAULT_COLOR
     */
    public void resetColor(){
        this.color = DEFAULT_COLOR;
    }

    /**
     * Sets the color of the tile to the ATTACKABLE_COLOR
     * tils of this color are attackable by the player.
     *
     */
    public void setColorToAttackable(){
        this.color = ATTACKABLE_COLOR;
    }

    /**
     * Sets the color of the tile to the ATTACKABLE_COLOR
     * the player can move to tiles with this color.
     */
    public void setColorToMovable(){
        this.color = MOVABLE_COLOR;
    }

    @Override
    public String toString() {
        return String.format("(%s,%s)", row, col);
    }


    public int getRow() {
        return row;
    }

    public int getCol(){
        return col;
    }
}
