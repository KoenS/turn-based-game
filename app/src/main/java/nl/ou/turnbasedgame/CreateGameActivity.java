package nl.ou.turnbasedgame;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

import nl.ou.turnbasedgame.game.models.GameLobby;
import nl.ou.turnbasedgame.game.models.player.AbstractPlayer;
import nl.ou.turnbasedgame.game.models.player.DragonPlayer;

/**
 * This activity is used to create a new game.
 */
public class CreateGameActivity extends AppCompatActivity {

    // UI elements
    private FrameLayout progressOverlay;
    private EditText gameNameField;
    private EditText dragonNameField;
    private EditText descriptionField;
    private Button startButton;

    // Firebase elements
    private DatabaseReference mDatabase;
    private static final String GAMES = "games";
    private static final String PLAYERLIST = "playerList";
    private static final String PLAYERS = "players";
    private static final String ACTIONS = "actionList";

    // Logging elements
    private static final String TAG = "CreateGameActivity";

    /* Create activity *************************************************************************/

    /**
     * Initialize activity
     * @param savedInstanceState: Bundle
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_game);

        // Initiate UI elements
        initUI();
        Log.d(TAG, "UI initiated");

        // Initiate FB and binding to UI elements
        initFB();
        Log.d(TAG, "FB initiated");
    }

    /**
     * Initialize UI elements
     */
    @SuppressWarnings("Convert2Lambda")
    private void initUI() {
        // fix the text color of the system bar. This should be done in the styles of the base-library.
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);

        // bind UI elements
        progressOverlay = findViewById(R.id.progress_overlay);
        gameNameField = findViewById(R.id.fieldGameName);
        dragonNameField = findViewById(R.id.fieldDragonName);
        descriptionField = findViewById(R.id.fieldDescription);
        startButton = findViewById(R.id.buttonCreateGame);

        // bind on click event
        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startGame();
            }
        });
    }

    /**
     * Initialize Firebase and FireBaseRecyclerAdapter
     */
    private void initFB() {
        //mDatabase = FirebaseDatabase.getInstance().getReference().child(GAMES);
        mDatabase = FirebaseDatabase.getInstance().getReference();
    }

    /* actions *********************************************************************************/

    /**
     * Attempt to create a new game with all required firebase objects.
     */
    private void startGame() {
        final String gameName = gameNameField.getText().toString();
        final String dragonName = dragonNameField.getText().toString();
        final String desciption = descriptionField.getText().toString();

        // check gameName and dragonName for validity using bitwise inclusive OR.
        if (!checkName(gameNameField) | (!checkName(dragonNameField))) {
            return;
        }

        // Disable button so there are no multi-posts
        setEditingEnabled(false);

        Toast.makeText(this, getString(R.string.toast_starting_game), Toast.LENGTH_SHORT).show();

        // Retrieve all active games to check if the gameName already exists
        mDatabase.child(GAMES)
                .orderByChild("active")
                .equalTo(true)
                .addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Log.d(TAG, "When searching for active games, the following snapshot was found: " + dataSnapshot.toString());

                // If there is no game with the same gameName, create a new game, else show an error.
                if (nameIsUnique(dataSnapshot, gameName)) {
                    // Create new game at /games/$gameid and /actionList/$gameid simultaneously
                    String gameKey = mDatabase.push().getKey();
                    Log.d(TAG, "gameKey of newly created game is: " + gameKey);

                    String playerKey = mDatabase.push().getKey();
                    Log.d(TAG, "playerKey of newly created player is: " + playerKey);

                    Map<String, Object> childUpdates = new HashMap<>();

                    GameLobby game = new GameLobby(gameName, gameKey, desciption);
                    Map<String, Object> gameValues = game.toMap();
                    childUpdates.put("/" + GAMES + "/" + gameKey, gameValues);

                    AbstractPlayer player = new DragonPlayer(dragonName);
                    Map<String, Object> playerValues = player.toMap();
                    childUpdates.put("/" + PLAYERLIST + "/" + gameKey + "/" + PLAYERS + "/" + playerKey, playerValues);

                    mDatabase.updateChildren(childUpdates);

                    // Start the GameActivity
                    try {
                        Log.d(TAG, "Game: " + gameName + " created. To battle!");

                        // pass gameKey to the GameActivity
                        Intent intent = new Intent(getBaseContext(), GameActivity.class);
                        intent.putExtra(GameActivity.EXTRA_GAME_KEY, gameKey);
                        intent.putExtra(GameActivity.EXTRA_PLAYER_NAME, dragonName);

                        startActivity(intent);
                    } catch(Exception e) {
                        Log.e(TAG, "Go to game failed", e);
                    }

                } else {
                    Log.i(TAG, "Game gameName " + gameName + " already exists");
                    gameNameField.setError(getString(R.string.name_already_exists));
                    setEditingEnabled(true);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e(TAG, "Oops, that's not supposed to happen.");
            }
        });
    }

    /**
     * Helper function to detect duplicate names
     * @param dataSnapshot: set of games
     * @param name: Name of the to-be-created game
     * @return: True if name already exists, else false.
     */
    private boolean nameIsUnique(DataSnapshot dataSnapshot, String name){
        boolean result = true;
        for (DataSnapshot actionDataSnapShot : dataSnapshot.getChildren()) {
            GameLobby game = actionDataSnapShot.getValue(GameLobby.class);
            if ((game != null) && (name.equals(game.getName()))) {
                result = false;
            }
        }
        return result;
    }

    /**
     * Helper function to enable/disable UI
     * @param enabled: enable UI if true, otherwise disable UI.
     */
    private void setEditingEnabled(boolean enabled) {
        // x.setEnabled(enabled) could be used, but this is considered more readable
        if (enabled) {
            startButton.setEnabled(true);
            gameNameField.setEnabled(true);
            progressOverlay.setVisibility(View.GONE);
        } else {
            startButton.setEnabled(false);
            gameNameField.setEnabled(false);
            progressOverlay.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Helper function to validate content of EditText field
     * @param field
     * @return
     */
    private boolean checkName(EditText field) {
        String name = field.getText().toString();
        if (TextUtils.isEmpty(name)) {
            field.setError(getString(R.string.name_required));
            return false;
        } else if ((name.length() < 3) || (name.length() > 15)) {
            field.setError(getString(R.string.name_length_3_15));
            return false;
        }
        return true;
    }
}
