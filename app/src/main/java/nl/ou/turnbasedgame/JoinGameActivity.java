package nl.ou.turnbasedgame;

import android.os.Bundle;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.view.View;
import android.preference.PreferenceManager;
import android.content.SharedPreferences;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

import nl.ou.turnbasedgame.game.models.player.AbstractPlayer;
import nl.ou.turnbasedgame.game.models.player.HumanPlayer;

/**
 * The join game activity attempts to join a game with the name provided by the settings.
 * If the name is already in use, the player will need to provide an alternative name.
 * loading screen is based on: https://medium.com/@tristanlarsin/android-ui-loading-screen-fec11bd86e99
 */
public class JoinGameActivity extends AppCompatActivity {

    // UI elements
    private FrameLayout progressOverlay;
    private EditText playerNameInput;
    private Button joinGameButton;

    // Firebase elements
    private DatabaseReference mDatabase;
    private static final String PLAYERLIST = "playerList";
    private static final String PLAYERS = "players";

    // Logging elements
    private static final String TAG = "JoinGameActivity";

    // Shared preferences and keys
    private static final String PLAYERNAME = "player_name";
    private static final String PLAYERKEY = "player_key";
    private SharedPreferences prefs;
    private String gameKey;
    private String playerName;
    private String playerKey;

    /* Create activity *************************************************************************/

    /**
     * Initialize activity
     * @param savedInstanceState: Bundle
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_join_game);

        // Load preferences
        initPrefs();
        Log.d(TAG, "Preferences loaded");

        // Initiate UI elements
        initUI();
        Log.d(TAG, "UI initiated");

        // Initiate FB and binding to UI elements
        initFB();
        Log.d(TAG, "FB initiated");

        // Load extras
        initExtras(savedInstanceState);

        playerNameInput.setText(playerName);

        // Automatically join the game (if possible): currently disabled
        // joinGame();
    }

    /**
     * Initialize UI elements
     */
    @SuppressWarnings("Convert2Lambda")
    private void initUI() {
        // fix the text color of the system bar. This should be done in the styles of the base-library.
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);

        // bind UI elements
        progressOverlay = findViewById(R.id.progress_overlay);
        playerNameInput = findViewById(R.id.playerName);
        joinGameButton = findViewById(R.id.joinButton);

        // bind on click event
        joinGameButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                joinGame();
            }
        });
    }

    /**
     * Initialize Firebase and FireBaseRecyclerAdapter
     */
    private void initFB() {
        mDatabase = FirebaseDatabase.getInstance().getReference();
    }

    /**
     * Initialize Preferences
     */
    private void initPrefs() {
        // @see https://stackoverflow.com/questions/2691772/android-preferences-how-to-load-the-default-values-when-the-user-hasnt-used-th
        PreferenceManager.setDefaultValues(this, R.xml.pref_general, false);
        prefs = PreferenceManager.getDefaultSharedPreferences(this);

        // Load player name and player key from preferences (if any)
        playerName = prefs.getString(PLAYERNAME, "@string/pref_default_player_name");
        playerKey = prefs.getString(PLAYERKEY, "");
    }

    /**
     * Initialize extras
     * @param savedInstanceState: Bundle
     */
    private void initExtras(Bundle savedInstanceState) {
        // @see https://stackoverflow.com/questions/5265913/how-to-use-putextra-and-getextra-for-string-data
        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                gameKey = null;
            } else {
                gameKey = extras.getString(GameActivity.EXTRA_GAME_KEY);
            }
        } else {
            gameKey = (String) savedInstanceState.getSerializable(GameActivity.EXTRA_GAME_KEY);
        }
    }

    /* actions *********************************************************************************/

    /**
     * Attempt to join a game
     */
    private void joinGame() {
        // check name for validity.
        if (!checkName(playerNameInput)) {
            return;
        }

        // Disable button so there are no multi-posts
        setEditingEnabled(false);

        // update the player name with the latest value
        playerName = playerNameInput.getText().toString();

        Toast.makeText(this, getString(R.string.toast_joining_game), Toast.LENGTH_SHORT).show();

        // Check for players in the same game with the same name.
        mDatabase.child(PLAYERLIST)
                 .child(gameKey)
                 .child(PLAYERS)
                 .orderByChild("name")
                 .equalTo(playerName)
                 .addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Log.d(TAG, "When searching for players with the same name, the following snapshot was found: " + dataSnapshot.toString());

                // Check if a new player, rejoining player or duplicate name
                if (!dataSnapshot.exists()) {
                    Log.d(TAG, "Playername " + playerName + " does not exist");
                    // If no players with the same name are found, the player is new.

                    // Add the player to the game and join the fray.
                    playerKey = mDatabase.push().getKey();
                    Map<String, Object> childUpdates = new HashMap<>();
                    Log.d(TAG, "key of newly created player is: " + playerKey);

                    // Create a new player and add it to the game.
                    AbstractPlayer player = new HumanPlayer(playerName);
                    Map<String, Object> playerValues = player.toMap();
                    childUpdates.put("/" + PLAYERLIST + "/" + gameKey + "/" + PLAYERS + "/" + playerKey, playerValues);

                    // Update Firebase
                    mDatabase.updateChildren(childUpdates);

                    // Store key in preferences.
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putString(PLAYERNAME, playerName);
                    editor.putString(PLAYERKEY, playerKey);
                    editor.commit();

                    // Start GameActivity
                    gotoGameActivity();
                } else if (rejoinGame(dataSnapshot, playerKey)) {
                    Log.d(TAG, "Playername " + playerName + " playerkey " + playerKey + " already exist");

                    // If a players with the same name is found, and the key matches, rejoin the game.
                    mDatabase.child(PLAYERLIST).child(gameKey).child(PLAYERS).child(playerKey).child("active").setValue(true);

                    // Start GameActivity
                    gotoGameActivity();
                } else {
                    Log.i(TAG, "Playername " + playerName + " already exists, but key does not match");

                    // Show error and enable UI
                    playerNameInput.setError(getString(R.string.name_already_exists));
                    setEditingEnabled(true);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e(TAG, "Oops, that's not supposed to happen.");
            }
        });
    }

    /**
     * Helper function to enable/disable UI
     * @param enabled: enable UI if true, otherwise disable UI.
     */
    private void setEditingEnabled(boolean enabled) {
        // x.setEnabled(enabled) could be used, but this is considered more readable
        if (enabled) {
            joinGameButton.setEnabled(true);
            playerNameInput.setEnabled(true);
            progressOverlay.setVisibility(View.GONE);
        } else {
            joinGameButton.setEnabled(false);
            playerNameInput.setEnabled(false);
            progressOverlay.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Helper function to validate content of EditText field
     * @param field
     * @return
     */
    private boolean checkName(EditText field) {
        String name = field.getText().toString();
        if (TextUtils.isEmpty(name)) {
            field.setError(getString(R.string.name_required));
            return false;
        } else if ((name.length() < 3) || (name.length() > 15)) {
            field.setError(getString(R.string.name_length_3_15));
            return false;
        }
        return true;
    }

    private boolean rejoinGame (DataSnapshot dataSnapshot, String key) {
        boolean result = false;
        for (DataSnapshot childDataSnapShot : dataSnapshot.getChildren()) {
            String keyFB = childDataSnapShot.getKey();
            result = key.equals(keyFB);
        }
        return result;
    }

    /**
     * Start the game activity
     */
    private void gotoGameActivity (    ) {
        try {
            Log.d(TAG, "Joining " + gameKey + ". To battle!");

            // Pass game key and player name to the GameActivity.
            Intent intent = new Intent(getBaseContext(), GameActivity.class);
            intent.putExtra(GameActivity.EXTRA_GAME_KEY, gameKey);
            intent.putExtra(GameActivity.EXTRA_PLAYER_NAME, playerName);

            startActivity(intent);
        } catch (Exception e) {
            Log.e(TAG, "Go to game failed", e);
        }
    }
}
