package nl.ou.turnbasedgame;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import nl.ou.turnbasedgame.databinding.ActivityGameBinding;
import nl.ou.turnbasedgame.game.models.actionlist.ActionList;
import nl.ou.turnbasedgame.game.models.player.AbstractPlayer;
import nl.ou.turnbasedgame.game.models.GameLobby;
import nl.ou.turnbasedgame.game.models.Game;
import nl.ou.turnbasedgame.game.views.GameSurface;
import nl.ou.turnbasedgame.game.views.surfacestate.AttackState;
import nl.ou.turnbasedgame.game.views.surfacestate.MoveState;
import nl.ou.turnbasedgame.util.observer.Observer;


public class GameActivity extends AppCompatActivity implements Observer {

    private static final String TAG = "GameActivity";
    public static final String EXTRA_GAME_KEY = "game_key";
    public static final String EXTRA_PLAYER_NAME = "player_name";

    public static final String GAME_SHAREDPREF_NAME = "CURRENT_GAME";


    private GameSurface gameSurface = null;
    private Game game;
    private String gameKey;
    private String playerName;

    private final String GAMELIST_PATHNAME = "games";

    //Binding between models and text elements in activity_game.xml
    private ActivityGameBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        // Set fullscreen
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_game);

        //Creates a binding between the layout and this activity
        binding = DataBindingUtil.setContentView(this, R.layout.activity_game);

        //Get the gameKey from the previous Activity
        gameKey = getIntent().getStringExtra(EXTRA_GAME_KEY);
        if (gameKey == null) {
            //throw new IllegalArgumentException("Must pass EXTRA_POST_KEY");
            Log.e(TAG, "Game not found: " + gameKey);
            gameKey = "DoS001";
        } else {
            Log.d(TAG, "Game started: " + gameKey);
        }

        //Get playerName from previous activity
        playerName = getIntent().getStringExtra(EXTRA_PLAYER_NAME);
        if (playerName == null) {
            //throw new IllegalArgumentException("Must pass EXTRA_POST_KEY");
            Log.e(TAG, "Player not found: " + playerName);
            playerName = "player1";
        } else {
            Log.d(TAG, "Player joined: " + playerName);
        }

        addCurrentGameToSharedPreferences();
        initGameFromFirebase(gameKey, playerName);
    }


    /**
     * Adds the game key and Player name to sharedPreference so that the player can
     * resume the game even when the application has been closed.
     */
    private void addCurrentGameToSharedPreferences(){
        SharedPreferences sharedPref  = getSharedPreferences(GAME_SHAREDPREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref .edit();
        //Store game name and player name in shared preference.
        editor.putString(EXTRA_PLAYER_NAME, playerName);
        editor.putString(EXTRA_GAME_KEY, gameKey);
        editor.commit();
    }

    /**
     * Remove game key and player name from shared preference.
     * When the player leaves a games. These should be removed from preferences.
     */
    private void removeCurrentGameFromSharedPreferences(){
        SharedPreferences sharedPref  = getSharedPreferences(GAME_SHAREDPREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref .edit();
        editor.remove(EXTRA_PLAYER_NAME);
        editor.remove(EXTRA_GAME_KEY);
        editor.commit();
    }



    /**
     * Binds the text elements in activity_game.xml to an player
     * @param player the player
     */
    public void bindPlayerToText(AbstractPlayer player){
        binding.setPlayer(player);
    }

    /**
     * Initializes the gameboard (grid)
     */
    private void initGameSurface(){
        gameSurface = findViewById(R.id.gameSurface);
        gameSurface.setGame(game);
        gameSurface.setGameActivity(this);
        game.registerObserver(gameSurface);
        game.registerObserver(this);
    }

    private void initGameFromFirebase(String gameKey, String playerName){
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference gameListRef = database.getReference(GAMELIST_PATHNAME);

        gameListRef.orderByKey().equalTo(gameKey).limitToFirst(1).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                GameLobby gameLobby = dataSnapshot.child(gameKey).getValue(GameLobby.class);
                if (gameLobby != null) {
                    Log.d(TAG, "Game " + gameKey + " retrieved: " + gameLobby.toString());
                    game = new Game(gameLobby, playerName);
                    game.setActionList(new ActionList(game));
                    game.joinGame();
                    initGameSurface();
                    initAttackButton();
                    initMoveButton();
                    initSkipTurnButton();
                    initTitleText();
                    gameListRef.removeEventListener(this);
                }else{
                    //If there is no game, then the game has been removed from the database.
                    //Send the player to the gameover Screen.
                    sendPlayerToGameScoreActivity();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    /**
     * The eventlistener should be removed when the activity is closed.
     */
    @Override
    protected void onDestroy(){
        super.onDestroy();
        if(game != null){
            game.removeActionListEventListeners();
        }
    }

    /**
     * A player performs a leave action by pressing the back button
     */
    @Override
    public void onBackPressed(){
        Log.d(TAG, "Back pressed. Leaving game.");
        game.createLeaveAction();
        finish();
    }

    private void initTitleText(){
        TextView gameTitle = findViewById(R.id.game_title);
        gameTitle.setText(game.getGameName());
    }

    private void initAttackButton(){
        Button attackButton = findViewById(R.id.attackButton);
        attackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                game.getGameboard().markAttackableTiles(game.getControlledPlayer());
                gameSurface.setSurfaceState(new AttackState());
                gameSurface.update();
            }
        });
    }

    private void initSkipTurnButton(){
        Button skipTurnButton = findViewById(R.id.skipTurnButton);
        skipTurnButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AbstractPlayer player = game.getControlledPlayer();
                game.createMoveAction(player.getRow(), player.getCol());
                gameSurface.update();
            }
        });

    }


    private void initMoveButton(){
        Button moveButton = findViewById(R.id.moveButton);

        moveButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                game.getGameboard().markMovableTiles(game.getControlledPlayer());
                gameSurface.setSurfaceState(new MoveState());
                gameSurface.update();
            }
        });
    }

    private void updateActionButtons(){
        Button attackButton = findViewById(R.id.attackButton);
        Button moveButton = findViewById(R.id.moveButton);
        Button skipTurnButton = findViewById(R.id.skipTurnButton);

        if(game.controllerCanPerformAction()){
            attackButton.setEnabled(true);
            moveButton.setEnabled(true);
            skipTurnButton.setEnabled(true);

        }else{
            attackButton.setEnabled(false);
            moveButton.setEnabled(false);
            skipTurnButton.setEnabled(false);

        }
    }


    private void checkPlayerStillInGame(){
        //game.getControlledPlayer() is null when this player has not yet been created.
        if(game.getControlledPlayer() != null){
            //if the playerList does not contain the controlled player, then the player is dead
            if(!game.getPlayers().contains(game.getControlledPlayer())){
                Log.d(TAG, "404 Player not found. Leaving game.");
                sendPlayerToGameScoreActivity();
            }
        }
    }

    private void sendPlayerToGameScoreActivity(){
        Intent intent = new Intent(getBaseContext(), GameScoreActivty.class);
        //Remove the current game from shared preferences.
        removeCurrentGameFromSharedPreferences();
        //Close the current game
        if(game != null){
            game.removeActionListEventListeners();
        }
        //Start the gameScoreActivity
        startActivity(intent);
        //Finish this activity
        finish();
    }

    /**
     * Updates the GUI when the gameState changes.
     * Gets called when the game receives an update.
     */
    public void update(){
        updateActionButtons();
        checkPlayerStillInGame();
        bindPlayerToText(game.getControlledPlayer());
    }
}
