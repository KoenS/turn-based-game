package nl.ou.turnbasedgame.game.views;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import nl.ou.turnbasedgame.GameActivity;
import nl.ou.turnbasedgame.game.models.Game;
import nl.ou.turnbasedgame.game.models.grid.Tile;
import nl.ou.turnbasedgame.game.models.player.AbstractPlayer;
import nl.ou.turnbasedgame.game.models.player.HumanPlayer;
import nl.ou.turnbasedgame.game.views.surfacestate.DefaultState;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class TestDefaultState {

    @Mock
    private Tile tile;

    @Mock
    private Game game;

    @Mock
    private GameSurface surface;

    @Mock
    private HumanPlayer player1;

    @Mock
    private GameActivity gameActivity;

    private DefaultState defaultState;


    @Before
    public void setUp(){
        defaultState = new DefaultState();
    }


    @Test
    public void testTouchPlayer(){

        int row = 1;
        int col = 2;

        //The playerlist contains 1 player
        List<AbstractPlayer> playerList = new ArrayList<>();
        playerList.add(player1);

        Mockito.when(game.getPlayers()).thenReturn(playerList);

        //The player clicks on a tile that is occupied by another player
        Mockito.when(player1.getRow()).thenReturn(row);
        Mockito.when(player1.getCol()).thenReturn(col);
        Mockito.when(tile.getRow()).thenReturn(row);
        Mockito.when(tile.getCol()).thenReturn(col);

        Mockito.when(surface.getGameActivity()).thenReturn(gameActivity);

        defaultState.handleOnTouch(tile, game, surface);

        //testing touching another player on the grid, should bind the touched player to the text
        // of the gameActivity.
        verify(gameActivity, times(1)).bindPlayerToText(player1);
    }


    @Test
    public void testNotTouchingPlayer(){

        int row = 1;
        int col = 2;

        //The playerlist contains 1 player
        List<AbstractPlayer> playerList = new ArrayList<>();
        playerList.add(player1);

        Mockito.when(game.getPlayers()).thenReturn(playerList);

        //The player clicks on a tile that is not occupied by another player
        Mockito.when(player1.getRow()).thenReturn(row);
        Mockito.when(tile.getRow()).thenReturn(row+1);

        Mockito.when(surface.getGameActivity()).thenReturn(gameActivity);

        defaultState.handleOnTouch(tile, game, surface);

        //testing touching another player on the grid, should bind the touched player to the text
        // of the gameActivity.
        verify(gameActivity, times(0)).bindPlayerToText(player1);
    }



}
