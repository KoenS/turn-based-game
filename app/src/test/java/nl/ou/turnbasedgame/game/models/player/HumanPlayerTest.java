package nl.ou.turnbasedgame.game.models.player;

import org.junit.Before;
import org.junit.Test;

import nl.ou.turnbasedgame.R;
import nl.ou.turnbasedgame.game.models.grid.Tile;
import nl.ou.turnbasedgame.game.models.player.HumanPlayer;

import static junit.framework.TestCase.assertEquals;


public class HumanPlayerTest {


    private static int defaultImageResource = R.drawable.ic_pikeman;
    private static int defaultControllerImageResource = R.drawable.ic_pikeman_green;

    private final String PLAYER_ONE_NAME = "player1";
    private final String PLAYER_TWO_NAME = "player2";

    private HumanPlayer player1, player2;

    @Before
    public void setUp() {

        //player1 is a controller
        player1 = new HumanPlayer(PLAYER_ONE_NAME, true);
        //player1 is a normal player
        player2 = new HumanPlayer(PLAYER_TWO_NAME, false);
    }

    @Test
    public void testImageResource(){
        assertEquals("player 1 should have the image resource for the controller",
                player1.getImageResource(),
                defaultControllerImageResource);

        assertEquals("player 1 should have the default image resource",
                player2.getImageResource(),
                defaultImageResource);
    }
}
