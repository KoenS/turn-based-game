package nl.ou.turnbasedgame.game.views;

import android.annotation.SuppressLint;
import android.support.v7.widget.RecyclerView;
import android.view.View;
//import android.widget.ImageView;
import android.widget.TextView;

import nl.ou.turnbasedgame.R;
import nl.ou.turnbasedgame.game.models.GameLobby;

/*
This class is used to bind an object (GameLobby) to an item of the recycler view (item_game).
The object is used in LobbyActivity to display the list of games.
 */

/**
 * GameViewHolder binds the view item to the model (GameLobby)
 */
public class GameViewHolder extends RecyclerView.ViewHolder {

    private final TextView gameName;
    private final TextView nrOfPlayersView;
    private final TextView gameDetailsView;
    //private ImageView gameView;

    /**
     * Constructor. Bind UI elements.
     * @param itemView: View
     */
    public GameViewHolder(View itemView) {
        super(itemView);

        gameName = itemView.findViewById(R.id.gameName);
        nrOfPlayersView = itemView.findViewById(R.id.gameNrOfPlayers);
        gameDetailsView = itemView.findViewById(R.id.gameDetails);

        //gameView = itemView.findViewById(R.id.gameAvatar);
    }

    /**
     * Bind view to model
     * @param game: GameLobby
     */
    @SuppressLint("SetTextI18n") // suppress warming about toString vs string.format
    public void bindToGame(GameLobby game) {
        gameName.setText(game.getName());
        nrOfPlayersView.setText(game.getNrOfPlayers().toString());

        // game details can be used to display whatever you want.
        gameDetailsView.setText(game.getDescription() + "\nCreated on: " + game.getHumanReadableCreatedTime());
    }
}

