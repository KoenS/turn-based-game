package nl.ou.turnbasedgame;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import nl.ou.applablib.AutoUpdate;
import nl.ou.applablib.ChangeLogSimple;

/**
 * Created by bvgastel on 18-01-2018.
 */

public class BaseActivity extends AppCompatActivity {
	private static final String VERSION = "0.1"; // als changelog weergegeven moet worden, moet je deze veranderen

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (BuildConfig.DEBUG && getIntent() != null && Intent.ACTION_MAIN.equals(getIntent().getAction()))
			AutoUpdate.checkNewVersion(this, getString(R.string.auto_update_url));


		//ChangeLogSimple cl = new ChangeLogSimple(this, R.raw.changelog, BaseActivity.VERSION);
		//if ((!cl.firstRunEver() && cl.firstRun()))
		//	cl.getLogDialog(0).show();

	}
}
