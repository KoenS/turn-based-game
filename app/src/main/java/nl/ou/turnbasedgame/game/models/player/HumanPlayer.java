package nl.ou.turnbasedgame.game.models.player;

import nl.ou.turnbasedgame.R;
import nl.ou.turnbasedgame.game.models.Game;
import nl.ou.turnbasedgame.game.models.player.AbstractPlayer;

public class HumanPlayer extends AbstractPlayer {

    private static int defaultImageResource = R.drawable.ic_pikeman;
    private static int defaultControllerImageResource = R.drawable.ic_pikeman_green;

    public HumanPlayer(String name, boolean isController){
        super(name);
        if (isController) {
            imageResource = defaultControllerImageResource;
        } else {
            imageResource = defaultImageResource;
        }
    }

    public HumanPlayer(String name){
        super(name);
    }
}
