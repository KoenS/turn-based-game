package nl.ou.turnbasedgame.game.models.actionlist;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.UUID;

import nl.ou.turnbasedgame.game.models.Game;


/**
 * A class that represents the actionList by the game.
 * The actionlist is stored in a firebase database.
 * This class is responsible for pulling all actions from the database and putting new actions
 * inside the database.
 * <p>
 * The actionList is a sequential list of all actions performed in the game.
 * By performing all the actions one by one, we can get the current state of the game.
 * <p>
 * Actions can not be
 */
public class ActionList {

    private final String LOG_TAG = "ActionList";


    private Game game;

    private FirebaseDatabase database = FirebaseDatabase.getInstance();
    private DatabaseReference valueRef;

    //String constants used find the right location in the database.
    private static String ACTIONLIST_PATHNAME = "actionList/";
    private static String ACTIONS_PATHNAME = "actions/";

    //The last action that was processed by the database.
    private Action lastAction;

    //A boolean indicating whether the game is reloading.
    // The actionList should not process any actions when the game is reloading.
    private boolean gameReloadTriggerd = false;


    /**
     * The ChildEventListener that checks whether an action has been added to the actionList.
     */
    private ChildEventListener actionListChangedEventListener = new ChildEventListener() {
        @Override
        public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
            Action action = dataSnapshot.getValue(Action.class);
            processAction(action);
        }

        @Override
        public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
            // We don't allow changes to action's within the actionList but this event still gets
            // triggered. When a player creates an action, the current time is used as a timestamp.
            // When the action is send to the database, this timestamp is changed to the server time.
            // This triggers this onChildChanged event.
            // We can use this to check for a desync. When another player adds an action between
            // those 2 events, we know that the actionList is out of order. At this point we should
            // reload the game.
            Action action = dataSnapshot.getValue(Action.class);
            if(action != null) {
                if(!lastAction.equals(action)){
                    Log.d(LOG_TAG, "lastAction does not equal action");
                    game.reloadGame();
                }
            }
        }

        @Override
        public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
            // We don't allow the removal of elements from the actionList. But if it somehow
            // happened (Like a manual removal from the database), then we should reload the game.
            Log.d(LOG_TAG, "actionListChangedEventListeneron ChildRemoved");
            game.reloadGame();
        }

        @Override
        public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
            // Nothing should happen when a child's priority has changed.
            Log.d(LOG_TAG, "actionListChangedEventListener onChildMoved");
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {
            //When an error happens, we could just reload the game
            Log.d(LOG_TAG, "actionListChangedEventListener onCancelled");
            game.reloadGame();
        }
    };

    /**
     * ValueEventListener that gets all the actions in one go.
     * This should be used when (re)loading
     * the game.
     */
    private ValueEventListener reloadGameEventListener = new ValueEventListener(){
        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

            //Notify the game that it's loading the actionList
            game.startLoadingActionList();

            Iterator actions = dataSnapshot.getChildren().iterator();
            while (actions.hasNext()) {
                DataSnapshot actionDataSnapShot = (DataSnapshot) actions.next();
                Action action = actionDataSnapShot.getValue(Action.class);
                processAction(action);
            }

            //Notify the game that it's finished the actionList
            game.finishedLoadingActionList();

            //When done, remove this eventListener
            valueRef.removeEventListener(this);
            //add the childeventlistener
            addChildEventListener();


        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {
            Log.d(LOG_TAG, "reloadEventListener onCancelled");
            game.reloadGame();
        }
    };


    private long timeStampLastAction = 0L;

    //Database Field name constants
    private final String TIMESTAMP = "timeStamp";
    private final String PLAYER = "player";
    private final String ACTIONTYPE = "actionType";
    private final String ROW = "row";
    private final String COL = "col";
    private final String ID = "id";

    /**
     * The constructor of the ActionList
     * @param game the game that belongs to this actionList.
     */
    public ActionList(Game game) {
        this.game = game;
        //The path where the actionlist is located in the database.
        valueRef = database.getReference(ACTIONLIST_PATHNAME + game.getActionListName() + "/" + ACTIONS_PATHNAME);
        initActionList();
    }


    private void initActionList() {
        addReloadEventListener();
    }


    /**
     * Add an action to the database.
     * @param playerName the name of the player that performs the action
     * @param actionType the actionType of the action
     * @param row the row where the action will be performed.
     * @param col the column where the action will be performed.
     */
    public void addAction(String playerName, ActionType actionType, int row, int col) {
        Map<String,Object> map = new HashMap<>();
        map.put(ID, UUID.randomUUID().toString());
        map.put(TIMESTAMP, ServerValue.TIMESTAMP);
        map.put(PLAYER, playerName);
        map.put(ACTIONTYPE, actionType);
        map.put(ROW, row);
        map.put(COL, col);
        valueRef.push().setValue(map);
    }

    /**
     * Should be triggered when the game is reloading.
     * This stops the processing of any actions by the eventlisteners.
     */
    public void gameReloadTriggerd(){
        gameReloadTriggerd = true;
    }


    /**
     * Gets the type of the action and lets the game perform the action.
     * @param action the action that needs to be performed.
     */
    private void processAction(Action action) {
        try {
            if (lastAction != null) {
                //If the game is reloading, we can't perform the action.
                if (gameReloadTriggerd) {
                    return;
                }

                //Do not repeat the same action
                if (action.equals(lastAction)) {
                    return;
                }
                //If the timestamp of a new action is smaller then the timestamp of the last action,
                //then the action were processed in the wrong order.
                // The game is out of sync and needs to be reloaded.
                if (action.getTimeStamp() < lastAction.getTimeStamp()) {
                    game.reloadGame();
                    return;
                }
            }

            lastAction = action;
            switch (action.getActionType()) {
                case ATTACK:
                    Log.d(LOG_TAG, "Attack action performed. timeStamp: " + action.getTimeStamp());
                    game.performAttackAction(action);
                    break;
                case JOIN:
                    Log.d(LOG_TAG, "Join action Performed. timeStamp: " + action.getTimeStamp());
                    game.performJoinAction(action);
                    break;
                case MOVE:
                    Log.d(LOG_TAG, "MOVE action Performed timeStamp: " + action.getTimeStamp());
                    game.performMoveAction(action);
                    break;
                case LEAVE:
                    Log.d(LOG_TAG, "LEAVE action performed timeStamp: " + action.getTimeStamp());
                    game.performLeaveAction(action);
                    break;
            }
        } catch (NullPointerException e) {
            // This may happen when running tests.
            Log.d(LOG_TAG, "Action removed during processing.");
        }
    }

    /**
     * Creates an Asynchronous listener that listens to changes in the database.
     */
    private void addChildEventListener() {
        Log.i(LOG_TAG, "addChildEventListener");
        try {
            valueRef.orderByChild("timeStamp").startAt(lastAction.getTimeStamp() + 1).addChildEventListener(actionListChangedEventListener);
        } catch (NullPointerException e) {
            // This may happen when running tests.
            Log.d(LOG_TAG, "Action removed during processing.");
        }
    }

    private void addReloadEventListener(){
        Log.i(LOG_TAG, "addReloadEventListener");
        valueRef.orderByChild("timeStamp").addValueEventListener(reloadGameEventListener);

    }

    public void removeChildEventListener(){
        valueRef.removeEventListener(actionListChangedEventListener);
    }
}

