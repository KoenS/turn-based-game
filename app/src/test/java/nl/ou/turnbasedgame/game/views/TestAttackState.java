package nl.ou.turnbasedgame.game.views;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import nl.ou.turnbasedgame.game.models.Game;
import nl.ou.turnbasedgame.game.models.grid.Grid;
import nl.ou.turnbasedgame.game.models.grid.Tile;
import nl.ou.turnbasedgame.game.views.surfacestate.AttackState;
import nl.ou.turnbasedgame.game.views.surfacestate.DefaultState;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class TestAttackState {

    @Mock
    private Tile tile;

    @Mock
    private Game game;

    @Mock
    private GameSurface surface;

    private AttackState attackState;

    @Mock
    private Grid gameboard;




    @Before
    public void setUp(){
        attackState = new AttackState();
    }

    @Test
    public void TestValidAttack() {
        int row = 1;
        int col = 2;

        Mockito.when(game.isValidAttackTargetForControlledPlayer(tile)).thenReturn(true);
        Mockito.when(tile.getRow()).thenReturn(row);
        Mockito.when(tile.getCol()).thenReturn(col);

        Mockito.when(game.getGameboard()).thenReturn(gameboard);

        attackState.handleOnTouch(tile, game, surface);

        verify(game, times(1)).createAttackAction(row, col);
        verify(gameboard, times(1)).resetGrid();

        verify(surface, times(1)).setSurfaceState(any(DefaultState.class));
    }

    @Test
    public void TestInValidAttack() {
        Mockito.when(game.isValidAttackTargetForControlledPlayer(tile)).thenReturn(false);

        Mockito.when(game.getGameboard()).thenReturn(gameboard);

        attackState.handleOnTouch(tile, game, surface);

        //No attack action should be created when a move is invalid.
        verify(game, times(0)).createAttackAction(anyInt(), anyInt());

        verify(gameboard, times(1)).resetGrid();

        verify(surface, times(1)).setSurfaceState(any(DefaultState.class));
    }



}
