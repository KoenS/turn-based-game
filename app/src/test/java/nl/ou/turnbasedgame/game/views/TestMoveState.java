package nl.ou.turnbasedgame.game.views;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import nl.ou.turnbasedgame.GameActivity;
import nl.ou.turnbasedgame.game.models.Game;
import nl.ou.turnbasedgame.game.models.grid.Grid;
import nl.ou.turnbasedgame.game.models.grid.Tile;
import nl.ou.turnbasedgame.game.models.player.HumanPlayer;
import nl.ou.turnbasedgame.game.views.surfacestate.DefaultState;
import nl.ou.turnbasedgame.game.views.surfacestate.MoveState;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class TestMoveState {
    @Mock
    private Tile tile;

    @Mock
    private Game game;

    @Mock
    private GameSurface surface;

    @Mock
    private HumanPlayer player1;

    @Mock
    private GameActivity gameActivity;

    @Mock
    private Grid gameboard;

    private MoveState moveState;


    @Before
    public void setUp(){
        moveState = new MoveState();
    }

    @Test
    public void TestValidMove(){
        int row = 1;
        int col = 2;

        Mockito.when(game.isValidMoveForControlledPlayer(tile)).thenReturn(true);
        Mockito.when(tile.getRow()).thenReturn(row);
        Mockito.when(tile.getCol()).thenReturn(col);

        Mockito.when(game.getGameboard()).thenReturn(gameboard);

        moveState.handleOnTouch(tile, game, surface);

        //When the move is valid it should create a move action
        verify(game, times(1)).createMoveAction(row, col);

        //The gameboard should be reset.
        verify(gameboard, times(1)).resetGrid();
        verify(surface, times(1)).setSurfaceState(any(DefaultState.class));
    }

    @Test
    public void TestInValidMove(){
        Mockito.when(game.isValidMoveForControlledPlayer(tile)).thenReturn(false);

        Mockito.when(game.getGameboard()).thenReturn(gameboard);

        moveState.handleOnTouch(tile, game, surface);

        //When the move is invalid it should not create a move action
        verify(game, times(0)).createMoveAction(anyInt(), anyInt());

        //The gameboard should be reset.
        verify(gameboard, times(1)).resetGrid();
        verify(surface, times(1)).setSurfaceState(any(DefaultState.class));

    }
}


