package nl.ou.turnbasedgame.util.observer;

/**
 * Observer interface used for the Observer-pattern.
 */
public interface Observer {
    public void update();
}
