package nl.ou.turnbasedgame.game.models.grid;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class TileTest {

    private Tile tile;

    @Before
    public void init() {
        tile = new Tile(1,2);
    }

    @Test
    public void testDefaultGetColor(){
        assertEquals("A tile should by default have the default color", Tile.DEFAULT_COLOR, tile.getColor());
    }

    @Test
    public void testSetColorToAttackable(){
        tile.setColorToAttackable();
        assertEquals("A tile should have ATTACKABLE_COLOR as color after it has been set to attackable ", Tile.ATTACKABLE_COLOR, tile.getColor());
    }

    @Test
    public void testSetColorToMovable(){
        tile.setColorToMovable();
        assertEquals("A tile should have MOVABLE_COLOR as color after it has been set to attackable ", Tile.MOVABLE_COLOR, tile.getColor());
    }

    @Test
    public void testResetColor(){
        tile.setColorToAttackable();
        tile.resetColor();
        assertEquals("A tile should have the default color after a reset", Tile.DEFAULT_COLOR, tile.getColor());
    }

    @Test
    public void testGetRow(){
        assertEquals("getRow should yield 1", 1, tile.getRow());
    }

    @Test
    public void testGetCol(){
        assertEquals("getRow should yield 1", 2, tile.getCol());
    }

    @Test
    public void testToString(){
        assertEquals("The string reprensentation of the tile should be (1,2)","(1,2)",tile.toString());
    }



}

