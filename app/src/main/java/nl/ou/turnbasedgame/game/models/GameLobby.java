package nl.ou.turnbasedgame.game.models;

import android.util.Log;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.ServerValue;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * GameLobby is the model for a game, primarily used by the lobby.
 * FirebaseRecyclerOptions binds the model to firebase.
 * FirebaseRecyclerAdapter bind the model to the view.
 * When firebase is updated, the view will be updated as well and vice-versa.
 */
public class GameLobby {
    private String actionList;
    @SuppressWarnings("unused")
    private Long created;  // Not yet used. See issue #69
    private String name;
    private Integer nrOfPlayers;
    private String description = "";
    private boolean active;

    // Logging elements
    private static final String TAG = "GameLobby";

    /**
     * default constructor required for DataSnapshot.getValue(GameLobby.class)
     */
    public GameLobby(){
        // do nothing.
    }

    /**
     * Constructor to create a new GameLobby object which can be written to firebase.
     * @param name: name of the game
     * @param key: key of the actionlist
     * @param description: description of the game
     */
    public GameLobby(String name, String key, String description){
        this.name = name;
        this.nrOfPlayers = 0;
        this.actionList = key;
        this.active = true;
        this.description = description;
        //this.created = ServerValue.TIMESTAMP; // Not yet used. See issue #69
    }

    /**
     * get method
     * @return name
     */
    public String getName() {
        return this.name;
    }

    /**
     *  get method
     * @return key of the games's actionlist
     */
    public String getActionList(){
        return this.actionList;
    }

    /**
     * get method
     * @return created date. (open issue: #69)
     */
    public Long getCreated(){
        return this.created;
    }

    /**
     * get method
     * @return active state
     */
    public Boolean getActive(){
        return this.active;
    }

    /**
     * get method
     * @return Number of active players
     */
    public Integer getNrOfPlayers(){
        return this.nrOfPlayers;
    }

    /**
     * set method
     * @param active: true if game is active. Otherwise false.
     */
    public void setActive(boolean active){
        this.active = active;
    }

    /**
     * set method
     * @param nrOfPlayers: the number of active players.
     */
    public void setNrOfPlayers(int nrOfPlayers){
        this.nrOfPlayers = nrOfPlayers;
    }

    /**
     * get method
     * @return description of the game
     */
    public String getDescription(){
        return this.description;
    }

    /**
     * set method
     * @param description: the description of the game.
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     *  Create a map of the GameLobby object.
     */
    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();

        result.put("active", active);
        result.put("name", name);
        result.put("created", ServerValue.TIMESTAMP); // Value should be assigned in constructor. See issue #69
        result.put("nrOfPlayers", nrOfPlayers);
        result.put("actionList", actionList);
        result.put("description", description);

        Log.d(TAG, "GameLobby mapped to: " + result);

        return result;
    }


    /**
     * Gets the date and time at which the game
     * @return a string representing the time and date at which the game was created.
     */
    public String getHumanReadableCreatedTime() {
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(created);
        Date d = c.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        return sdf.format(d);
    }
}
