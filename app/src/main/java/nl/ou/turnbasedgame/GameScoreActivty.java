package nl.ou.turnbasedgame;

import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class GameScoreActivty extends AppCompatActivity {

    private static final String TAG = "GameScoreActivity";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "OnCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_score);
        initBackToLobbyButton();
    }

    private void startLobbyActivity(){
        Intent intent = new Intent(getBaseContext(), LobbyActivity.class);
        startActivity(intent);
    }


    @Override
    public void onBackPressed(){
        startLobbyActivity();
    }

    private void initBackToLobbyButton(){
        Button backToLobbyButton = findViewById(R.id.backToLobbyButton);
        backToLobbyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startLobbyActivity();
            }
        });


    }
}
