package nl.ou.turnbasedgame.game.views.surfacestate;

import java.util.List;

import nl.ou.turnbasedgame.game.models.player.AbstractPlayer;
import nl.ou.turnbasedgame.game.models.Game;
import nl.ou.turnbasedgame.game.models.grid.Tile;
import nl.ou.turnbasedgame.game.views.GameSurface;

public class DefaultState implements SurfaceState{
    /**
     * Handles the touchevent when the gameSurface is in the default state.
     *
     * When the player touches a tile that contains another player, then the game should show
     * information about that player
     *
     * When the player touches a tile that contains no other player, then the game should show
     * information about the controlled player.
     *
     * @param tile the tile that has been touched
     * @param game the game object
     * @param surface the gamesurface
     */
    @Override
    public void handleOnTouch(Tile tile, Game game, GameSurface surface) {
        //Should show player information when clicking on a player

        List<AbstractPlayer> players = game.getPlayers();
        AbstractPlayer selectedPlayer = null;
        for(AbstractPlayer player: players){
            if(player.getRow() == tile.getRow() && player.getCol() == tile.getCol()){
                selectedPlayer = player;
                break;
            }
        }

        if(selectedPlayer != null) {
            surface.getGameActivity().bindPlayerToText(selectedPlayer);
        }else{
            surface.getGameActivity().bindPlayerToText(game.getControlledPlayer());
        }
    }
}
