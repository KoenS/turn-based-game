package nl.ou.turnbasedgame;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.app.AppCompatActivity;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import nl.ou.turnbasedgame.game.models.GameLobby;
import nl.ou.turnbasedgame.game.views.GameViewHolder;

/**
 * Lobby activity uses a FirebaseRecyclerAdapter to bind firebase objects directly to the UI.
 * A list of games is displayed and the user can select a game or create a new one.
 * The objects model is GameLobby
 * The object view is GameViewHolder (using item_game as layout)
 *
 * https://github.com/firebase/FirebaseUI-Android/blob/master/database/README.md
 */
public class LobbyActivity extends AppCompatActivity {

    // UI elements
    private FloatingActionButton fabButton;

    // Firebase UI elements
    private FirebaseRecyclerAdapter<GameLobby, GameViewHolder> mAdapter;
    private RecyclerView mRecycler;
    private LinearLayoutManager mManager;

    // Firebase elements
    private DatabaseReference mDatabase;
    private static final String GAMES = "games";
    private static final String ACTIVE = "active";

    // Logging elements
    private static final String TAG = "LobbyActivity";

    /* Create activity *************************************************************************/

    /**
     * Initialize activity
     * @param savedInstanceState: Bundle
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (BuildConfig.DEBUG) {
            StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
                    .detectDiskReads()
                    .detectDiskWrites()
                    .detectNetwork()   // or .detectAll() for all detectable problems
                    .penaltyLog()
                    .build());
            StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
                    .detectLeakedSqlLiteObjects()
                    .detectLeakedClosableObjects()
                    .penaltyLog()
                    .penaltyDeath()
                    .build());
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lobby);

        // Initiate UI elements
        initUI();
        Log.d(TAG, "Lobby UI initiated");

        // Initiate FB and binding to UI elements
        initFB();
        Log.d(TAG, "Lobby FB initiated");

        // Attach the Adapter to the RecyclerView
        mRecycler.setAdapter(mAdapter);
    }

    /**
     * Initialize UI elements
     */
    @SuppressWarnings("Convert2Lambda") // suppresses warning about use of onClick
    private void initUI() {
        // fix the text color of the system bar. This should be done in the styles of the base-library.
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);

        // the example uses rootView instead of RecyclerView. But this seems to work better.
        mRecycler = findViewById(R.id.gamesList);
        mRecycler.setHasFixedSize(true);

        // create layout manager and recycler view
        mManager = new LinearLayoutManager(this);
        mRecycler.setLayoutManager(mManager);

        // floating button to create a new games.
        fabButton = findViewById(R.id.fabNewGame);

        // bind on click event
        fabButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LobbyActivity.this, CreateGameActivity.class));
            }
        });
    }

    /**
     * Initialize Firebase and FireBaseRecyclerAdapter
     */
    private void initFB() {
        // create database reference
        mDatabase = FirebaseDatabase.getInstance().getReference();

        // Set up FirebaseRecyclerAdapter with the Query
        Query gamesQuery = mDatabase.child(GAMES).orderByChild(ACTIVE).equalTo(true);
        FirebaseRecyclerOptions options = new FirebaseRecyclerOptions.Builder<GameLobby>()
                .setQuery(gamesQuery, GameLobby.class)
                .build();

        // Create an adapter to bind the model of a game to the view of a game.
        mAdapter = new FirebaseRecyclerAdapter<GameLobby, GameViewHolder>(options) {

            @Override
            public GameViewHolder onCreateViewHolder(ViewGroup parent, int i) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_game, parent, false);
                return new GameViewHolder(view);
            }

            @Override
            @SuppressWarnings("Convert2Lambda") // suppresses warning about use of onClick
            @SuppressLint("RecyclerView") // suppresses warning about use of position
            protected void onBindViewHolder(GameViewHolder viewHolder, int position, final GameLobby game) {
                // Bind the game object to the game holder
                final DatabaseReference gameRef = getRef(position);
                Log.d(TAG, "Game: " + game.getName() + " Bound on location " + position);

                // Set click listener for the game
                final String gameKey = gameRef.getKey();
                viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                    // Launch JoinGameActivity upon selecting a game.
                    @Override
                    public void onClick(View v) {
                        try {
                            Log.d(TAG, "Join Game: " + game.getName() + " (" + position + ") selected. To join game screen!");
                            Intent intent = new Intent(getBaseContext(), JoinGameActivity.class);
                            // Pass gameKey to the JoinGameActivity using putExtra
                            intent.putExtra(GameActivity.EXTRA_GAME_KEY, gameKey);
                            startActivity(intent);
                        } catch(Exception e) {
                            Log.e(TAG, "Join Game", e);
                        }
                    }
                });

                // Bind Game to ViewHolder
                viewHolder.bindToGame(game);
            }

            @Override
            public void onDataChanged() {
                // Called each time there is a new data snapshot. You may want to use this method
                // to hide a loading spinner or check for the "no documents" state and update your UI.
                // ...
            }

            @Override
            public void onError(DatabaseError e) {
                Log.e(TAG, "Error getting data", e.toException());
            }
        };
    }

    /* Start/Stop activity *********************************************************************/

    /**
     * Start the activity and firebase listener
     */
    @Override
    public void onStart() {
        super.onStart();
        mAdapter.startListening();
        Log.d(TAG, "Lobby activity started");
    }

    /**
     * Stop the activity and firebase listener
     */
    @Override
    public void onStop() {
        super.onStop();
        mAdapter.stopListening();
        Log.d(TAG, "Lobby activity stopped");
    }
}
