package nl.ou.turnbasedgame.game.models.actionlist;

/**
 * This class represents an action that can be performed in the game.
 */
public class Action {

    private ActionType actionType;
    private int col;
    private int row;
    private String player;
    private long timeStamp;
    private String id;

    public Action() {

    }

    /**
     * The constructor of an Action
     * @param actionType the action type
     * @param col the column the action needs to be performed on
     * @param row the row the action needs to be performed on
     * @param player the player that performs the action
     * @param timeStamp the time at which the action is performed
     */
    public Action(ActionType actionType, String id, int col, int row, String player, long timeStamp) {
        this.actionType = actionType;
        this.id = id;
        this.col = col;
        this.row = row;
        this.player = player;
        this.timeStamp = timeStamp;
    }

    public void setActionType(ActionType actionType) {
        this.actionType = actionType;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setCol(int col) {
        this.col = col;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public void setPlayer(String playerName) {
        this.player = playerName;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }


    public ActionType getActionType() {
        return actionType;
    }

    public String getId(){return id;}

    public int getCol() {
        return col;
    }

    public int getRow() {
        return row;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public String getPlayer() {
        return player;
    }


    /**
     * Two actions are equal when the have the same id.
     * @param obj an Action
     * @return a boolean indicating whether the actions are the same action.
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        if (obj instanceof Action) {
            final Action action = (Action)obj;
            return action.getId().equals(getId());
        }
        return false;
    }
}
