package nl.ou.turnbasedgame;


import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.preference.PreferenceManager;
import android.support.test.espresso.ViewAction;
import android.support.test.espresso.action.CoordinatesProvider;
import android.support.test.espresso.action.GeneralClickAction;
import android.support.test.espresso.action.Press;
import android.support.test.espresso.action.Tap;
import android.support.test.espresso.core.internal.deps.guava.collect.Iterables;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.runner.lifecycle.ActivityLifecycleMonitorRegistry;
import android.support.test.runner.lifecycle.Stage;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;

import static android.support.test.InstrumentationRegistry.getInstrumentation;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.*;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.*;

import nl.ou.turnbasedgame.game.models.GameLobby;
import nl.ou.turnbasedgame.game.models.player.AbstractPlayer;;
import nl.ou.turnbasedgame.game.models.player.DragonPlayer;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static org.hamcrest.Matchers.allOf;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class GameTest {

    FirebaseDatabase db;
    List<String> testGameKeys = new ArrayList<>();
    String playerKey;
    String gameKey;
    String gameName;

    //Database Field name constants
    private final String TIMESTAMP = "timeStamp";
    private final String PLAYER = "player";
    private final String ACTIONTYPE = "actionType";
    private final String ROW = "row";
    private final String COL = "col";
    private final String ID = "id";

    private static final String PLAYERNAME = "player_name";

    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void gameTest() {
        String gameName = createGame();
        String playerName = getPlayerName();

        // Click the LobbyActivity button to go to the lobby.
        // @todo: Load text from strings.xml.
        onView(allOf(withText("Go To Game Lobby"), isDisplayed())).perform(click());
        sleep();

        // Click on the game in the lobby.
        onView(allOf(withText(gameName), isDisplayed())).perform(click());
        sleep();

        // Click Join game button.
        onView(allOf(withText("Join Game"), isDisplayed())).perform(click());
        sleep();

        // Check if we see our player name.
        onView(withId(R.id.text_player_name)).check(matches(withText("Player Name: " + playerName)));
        sleep();

        // Move our player to a fixed position.
        addAction(playerName, "MOVE", 4, 7);
        sleep();

        // Click the skip turn button. This is required because we already moved.
        onView(allOf(withText("Skip turn"), isDisplayed())).perform(click());
        sleep();

        // Move the dragon.
        addAction("Smaug", "MOVE", 3, 2);
        sleep();

        // Click move and click the next position.
        onView(allOf(withText("Move"), isDisplayed())).perform(click());
        sleep();
        tapGameSurface(4, 5);
        sleep();

        // Move the dragon.
        addAction("Smaug", "MOVE", 3, 4);
        sleep();

        // Click move and click the next position.
        onView(allOf(withText("Move"), isDisplayed())).perform(click());
        sleep();
        tapGameSurface(3, 5);
        sleep();

        addAction("Smaug", "ATTACK", 3, 5);
        sleep();

        // Attack each other 9 times more.
        for (int i = 0; i < 9; ++i) {
            onView(allOf(withText("Attack"), isDisplayed())).perform(click());
            sleep();
            tapGameSurface(3, 4);
            sleep();

            addAction("Smaug", "ATTACK", 3, 5);
            sleep();
        }

        // Check that we see the game over text.
        onView(withId(R.id.gameOverText)).check(matches(withText("Game Over")));
    }

    private void sleep() {
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {}
    }

    private void addAction(String playerName, String actionType, int row, int col) {
        DatabaseReference valueRef = db.getReference("/actionList/" + gameKey + "/actions");
        Map<String, Object> map = new HashMap<>();
        map.put(ID, UUID.randomUUID().toString());
        map.put(TIMESTAMP, ServerValue.TIMESTAMP);
        map.put(PLAYER, playerName);
        map.put(ACTIONTYPE, actionType);
        map.put(ROW, row);
        map.put(COL, col);
        valueRef.push().setValue(map);
    }

    private void tapGameSurface(int row, int col) {
        Display display = getActivityInstance().getWindowManager(). getDefaultDisplay();
        Point size = new Point();
        display. getSize(size);
        int width = size.x;
        onView(allOf(withId(R.id.gameSurface), isDisplayed())).perform(clickXY(
                width / 8  * col + width / 16,
                width / 8  * row + width / 16
        ));
    }

    /**
     * Create a test game.
     *
     * @return Game name
     */
    private String createGame() {
        gameName = "Test" + (int) (Math.random() * 1e5);

        // Code is copied from the CreateGameActivity.
        // @todo: Refactor create game code to a model class.

        DatabaseReference dbRoot = db.getReference();
        dbRoot.child("games");
        gameKey = dbRoot.push().getKey();
        playerKey = dbRoot.push().getKey();
        Map<String, Object> childUpdates = new HashMap<>();
        GameLobby game = new GameLobby(gameName, gameKey, "Test game");
        Map<String, Object> gameValues = game.toMap();
        childUpdates.put("/games/" + gameKey, gameValues);
        AbstractPlayer player = new DragonPlayer("Smaug");
        Map<String, Object> playerValues = player.toMap();
        childUpdates.put("/playerList/" + gameKey + "/players/" + playerKey, playerValues);
        dbRoot.updateChildren(childUpdates);
        testGameKeys.add(gameKey);
        sleep();

        addAction("Smaug", "JOIN", 3, 0);
        sleep();

        return gameName;
    }

    private String getPlayerName() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivityInstance());
        return prefs.getString(PLAYERNAME, "@string/pref_default_player_name");
    }

    @Before
    public void initDatabase() {
        db = FirebaseDatabase.getInstance();
    }

    @After
    public void cleanupTestGames() {
        initDatabase();
        sleep();
        testGameKeys.forEach((gameKey) -> {
            db.getReference("/games/" + gameKey).removeValue();
            db.getReference("/playerList/" + gameKey).removeValue();
        });
        testGameKeys.clear();
        sleep();
    }

    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }

    public static ViewAction clickXY(final int x, final int y) {
        // @see https://stackoverflow.com/questions/22177590/click-by-bounds-coordinates/22798043#22798043
        return new GeneralClickAction(
            Tap.SINGLE,
            new CoordinatesProvider() {
                @Override
                public float[] calculateCoordinates(View view) {

                    final int[] screenPos = new int[2];
                    view.getLocationOnScreen(screenPos);

                    final float screenX = screenPos[0] + x;
                    final float screenY = screenPos[1] + y;
                    float[] coordinates = {screenX, screenY};

                    return coordinates;
                }
            },
            Press.FINGER
        );
    }

    private Activity currentActivity = null;

    public Activity getActivityInstance() {
        // @see https://testyour.app/blog/get-current-activity
        getInstrumentation().runOnMainSync(new Runnable() {
            public void run() {
                Collection<Activity> resumedActivities =
                        ActivityLifecycleMonitorRegistry.getInstance().getActivitiesInStage(Stage.RESUMED);
                for (Activity activity: resumedActivities){
                    Log.d("Your current activity: ", activity.getClass().getName());
                    currentActivity = activity;
                    break;
                }
            }
        });
        return currentActivity;
    }
}
