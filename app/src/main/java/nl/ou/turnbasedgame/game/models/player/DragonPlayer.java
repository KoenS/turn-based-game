package nl.ou.turnbasedgame.game.models.player;

import android.graphics.Bitmap;

import nl.ou.turnbasedgame.R;
import nl.ou.turnbasedgame.game.models.Game;
import nl.ou.turnbasedgame.game.models.player.AbstractPlayer;

public class DragonPlayer extends AbstractPlayer {

    private static int defaultImageResource = R.drawable.ic_dragon_head;
    private static int defaultControllerImageResource = R.drawable.ic_dragon_head_green;


    public DragonPlayer(String name, boolean isController){
        super(name);
        if (isController) {
            imageResource = defaultControllerImageResource;
        }else{
            imageResource = defaultImageResource;
        }
    }


    public void setControllerImageResource(){
        this.imageResource = defaultControllerImageResource;
    }

    public DragonPlayer(String name){
        super(name);
    }
}
