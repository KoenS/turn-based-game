package nl.ou.turnbasedgame.game.models.grid;


import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import nl.ou.turnbasedgame.game.models.player.AbstractPlayer;
import nl.ou.turnbasedgame.game.models.player.HumanPlayer;

import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.Matchers.any;
import static org.hamcrest.Matchers.hasSize;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class GridTest {

    private Grid grid;

    @Mock
    private HumanPlayer player1, player2;

    private List<AbstractPlayer> playerList;


    @Before
    public void setUp(){
        playerList = new ArrayList<>();
        //add players to playerslist
        playerList.add(player1);
        playerList.add(player2);

        grid = new Grid(playerList);
    }

    @Test
    public void testGetTileList(){
        assertThat("The tile list should contain 8x8 = 64 items", grid.getTileList(), hasSize(64));

        //the first tile should be (0,0)
        assertEquals(grid.getTileList().get(0).getCol(),0);
        assertEquals(grid.getTileList().get(0).getRow(),0);

        //the last tile should be (7,7)
        assertEquals(grid.getTileList().get(63).getCol(),7);
        assertEquals(grid.getTileList().get(63).getRow(),7);
    }

    @Test
    public void testRowCount(){
        assertEquals("The grid should have 8 rows", grid.getRowCount(), 8);
    }

    @Test
    public void testColCount(){
        assertEquals("The grid should have 8 columns", grid.getColCount(), 8);
    }

    @Test
    public void testGetTile(){
        Tile tile = grid.getTile(1,2);
        assertEquals("The row of the tile should equal 1", tile.getRow(), 1);
        assertEquals("The col of the tile should equal 2", tile.getCol(), 2);
    }

    @Test
    public void testGetMovableTiles(){
        //player 1 has position (2,1)
        Mockito.when(player1.getCol()).thenReturn(1);
        Mockito.when(player1.getRow()).thenReturn(0);

        //player 2 has position (2,2)
        Mockito.when(player2.getCol()).thenReturn(1);
        Mockito.when(player2.getRow()).thenReturn(1);

        Mockito.when(player1.getMovement()).thenReturn(2);
        Mockito.when(player2.getMovement()).thenReturn(2);

        // There are 6 tiles within movement range of player 1.
        // 7 tiles within range.
        // 1 tile occupied by another player"
        assertThat("There are 6 tiles within movement range of player 1",
                grid.getMovableTiles(player1),
                hasSize(6));

        // There are 9 tiles within movement range of player 2.
        // 10 tiles within range.
        // 1 tile occupied by another player"
        assertThat("There are 9 tiles within movement range of player 1",
                grid.getMovableTiles(player2),
                hasSize(9));
    }

    @Test
    public void testGetAttackableTiles(){
        //player 1 has position (2,1)
        Mockito.when(player1.getCol()).thenReturn(1);
        Mockito.when(player1.getRow()).thenReturn(0);

        //player 2 has position (2,2)
        Mockito.when(player2.getCol()).thenReturn(2);
        Mockito.when(player2.getRow()).thenReturn(1);

        Mockito.when(player1.getRange()).thenReturn(2);
        Mockito.when(player2.getRange()).thenReturn(0);


        assertThat("Player1 can attack 1 tile",
                grid.getAttackableTiles(player1),
                hasSize(1));

        assertThat("Player2 can attack 0 tiles",
                grid.getAttackableTiles(player2),
                hasSize(0));
    }

    @Test
    public void testMarkAttackableTiles(){
        //player 1 has position (2,1)
        Mockito.when(player1.getCol()).thenReturn(1);
        Mockito.when(player1.getRow()).thenReturn(0);

        //player 2 has position (2,1)
        Mockito.when(player2.getCol()).thenReturn(2);
        Mockito.when(player2.getRow()).thenReturn(1);

        Mockito.when(player1.getRange()).thenReturn(2);
        Mockito.when(player2.getRange()).thenReturn(0);


        grid.markAttackableTiles(player1);

        //tile with row 1 and col 2 should be marked as attackable
        Tile tile = grid.getTile(1,2);
        assertEquals("This tile should be marked as attackable", Tile.ATTACKABLE_COLOR, tile.getColor());

        //player can not attack any tiles. All tiles should now be marked with the default color
        grid.markAttackableTiles(player2);
        assertEquals("This tile should now be with the default color", Tile.DEFAULT_COLOR, tile.getColor());
    }

    @Test
    public void testMarkMoveAbleTiles(){
        //player 1 has position (2,1)
        Mockito.when(player1.getCol()).thenReturn(1);
        Mockito.when(player1.getRow()).thenReturn(1);

        Mockito.when(player1.getMovement()).thenReturn(1);

        grid.markMovableTiles(player1);

        //tiles (0,1), (1,0), (2,1), (1,2) should now be marked as moveable
        Tile tile = grid.getTile(0,1);
        assertEquals("This tile should be marked as moveable", Tile.MOVABLE_COLOR, tile.getColor());

        tile = grid.getTile(1,0);
        assertEquals("This tile should be marked as moveable", Tile.MOVABLE_COLOR, tile.getColor());

        tile = grid.getTile(2,1);
        assertEquals("This tile should be marked as moveable", Tile.MOVABLE_COLOR, tile.getColor());

        tile = grid.getTile(1,2);
        assertEquals("This tile should be marked as moveable", Tile.MOVABLE_COLOR, tile.getColor());

        tile = grid.getTile(1,1);
        assertEquals("This tile is occupied by the player and should not be marked as movable", Tile.DEFAULT_COLOR, tile.getColor());

        tile = grid.getTile(3,1);
        assertEquals("This tile is outside of the movement range and should not be marked as moveable", Tile.DEFAULT_COLOR, tile.getColor());
    }


    @Test
    public void testGetRandomEmptyTile(){
        //player 1 has position (2,1)
        Mockito.when(player1.getCol()).thenReturn(1);
        Mockito.when(player1.getRow()).thenReturn(0);

        //player 2 has position (2,1)
        Mockito.when(player2.getCol()).thenReturn(2);
        Mockito.when(player2.getRow()).thenReturn(1);

        assertThat("It should return an object of the Tile class ",grid.getRandomEmptyTile(), any(Tile.class));
        assertThat("It should not return the tile (0,1) as it occupied by player 1", grid.getRandomEmptyTile(), not(grid.getTile(0,1)));
        assertThat("It should not return the tile (1,2) as it occupied by player 2", grid.getRandomEmptyTile(), not(grid.getTile(1,2)));
    }


    @Test
    public void testResetGrid(){
        //Set the color of a tile to movable color
        Tile tile = grid.getTile(1,1);
        tile.setColorToMovable();

        //After the grid has been reset, the color should be reset to default
        grid.resetGrid();
        assertEquals(tile.getColor(), Tile.DEFAULT_COLOR);

        //Set the color of a tile to attackable color
        tile = grid.getTile(2,2);
        tile.setColorToAttackable();

        //After the grid has been reset, the color should be reset to default
        grid.resetGrid();
        assertEquals(tile.getColor(), Tile.DEFAULT_COLOR);

    }





}
